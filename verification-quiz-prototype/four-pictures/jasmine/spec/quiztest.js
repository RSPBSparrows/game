describe("Quiz test", function() {

    // CHANGABLE VARIABLES
    //--------------------------------------------------------------------------
    var no_questions = 6; // number of questions
    var no_hard_questions = 1; // number of hard questions (< no_questions)
	var no_med_questions = 2; // number of medium questions (<no_questions)
    var pass_percent = 80; // pass mark as %
    var time_per_question = 4; // time allowed per question (seconds)

    // scores for each type of image
    var hs_hard_score = 2; // score per hard question correct
    var hs_easy_score = 1; // score per easy question correct
    var decoy_score = 0; // score for clicking a decoy bird
    var dud_score = -1; // score for clicking a dud bird
    //--------------------------------------------------------------------------

    var score = 0;
    // max possible score
    var max_score = (no_questions - no_hard_questions) * hs_easy_score + no_hard_questions * hs_hard_score;
    var question_no = -1;
    var questions;

    // create array of images
	
    function createQuestions() {
        // TODO
        used_files = [];
        questions = [];
        var images;
        for (i = 0; i < no_questions - (no_hard_questions - no_med_questions); i++) {
            images = [];
            images.push(new QuizImage("data/hs-easy/"+(i+1)+" resized.jpg", "hs_easy"));
            images.push(new QuizImage("data/hs-sim/"+1+" resized.jpg", "decoy"));
            images.push(new QuizImage("data/duds/"+(i+1)+" resized.jpg", "dud"));
            images.push(new QuizImage("data/duds/"+(i+2)+" resized.jpg", "dud"));
            shuffle(images);
            questions.push(new QuizQuestion(images, time_per_question));
        }
		
		for (i = 0; i < no_med_questions; i++) {
            images = [];
            images.push(new QuizImage("data/hs-easy/"+(i+1)+" resized.jpg", "hs_easy"));
            images.push(new QuizImage("data/hs-sim/"+1+" resized.jpg", "decoy"));
            images.push(new QuizImage("data/hs-sim/"+(i+1)+" resized blurred.jpg", "decoy"));
            images.push(new QuizImage("data/duds/"+(i+2)+" resized.jpg", "dud"));
            shuffle(images);
            questions.push(new QuizQuestion(images, time_per_question));
        }
		
        for (i = 0; i < no_hard_questions; i++) {
            images = [];
            images.push(new QuizImage("data/hs-hard/"+(i+1)+" resized.jpg", "hs_hard"));
            images.push(new QuizImage("data/hs-sim/"+1+" resized.jpg", "decoy"));
            images.push(new QuizImage("data/hs-sim/"+(i+1)+" resized blurred.jpg", "decoy"));
            images.push(new QuizImage("data/hs-sim/"+(i+2)+" resized blurred.jpg", "decoy"));
            shuffle(images);
            questions.push(new QuizQuestion(images, time_per_question));
        }
		return(images.length); //JUST FOR TESTING, PLEASE REMOVE OTHERWISE.
	}

    function shuffle(a) {
        var j, x, i;
        for (i = a.length - 1; i > 0; i--) {
            j = Math.floor(Math.random() * (i + 1));
            x = a[i];
            a[i] = a[j];
            a[j] = x;
        }
    }

    function QuizImage(filename, type) {
        this.filename = filename;
        this.type = type;

        this.getHTML = function() {
            return '<img src="' + this.filename + '">';
        };
    }

    function QuizQuestion(images, time) {
        this.images = images;
        this.time = time * 1000;
        this.active = false;

        this.display = function() {
            html = "";
            this.images.forEach(function(image, i) {
                if (i == 2) html += "</br>";
                html += image.getHTML();
            });
            $("div#image-frame").html(html);
            this.active = true;
            var self = this;
            setTimer(this.time / 1000 - 1, self);
            setTimeout(function() {
                if (self.active) {
                    self.active = false;
                    showNextQuestion();
                }
            }, this.time);
        };

        this.checkAnswer = function(ans) {
            this.active = false;
            answer = this.images[ans];
            if (answer.type === "hs_hard") return hs_hard_score;
            if (answer.type === "hs_easy") return hs_easy_score;
            if (answer.type === "decoy") return decoy_score;
            if (answer.type === "dud") return dud_score;
        };
    }

    function setTimer(seconds, question) {
        if (question.active) {
            $("div#timer").html("Time remaining: <br><span class='large'>" + seconds + "</span>");
            if (seconds > 0) {
                setTimeout(function() {
                    setTimer(seconds - 1, question);
                }, 1000);
            } else {
                $("div#timer").html("Time remaining: <br><span class='large'>0</span>");
            }
        }
    }

    function displayQuestionNo() {
        $("p#question-number").html("<h5>Question " + (question_no + 1) + " of " + no_questions + ":</h5>");
    }

    function showNextQuestion() {
        question_no++;
        if (question_no >= no_questions) {
            endQuiz();
            return false;
        }
        currentQuestion = questions[question_no];
        currentQuestion.display();
        displayQuestionNo();
		return true;
    }

    function endQuiz() {
        var finalscore = (score / max_score) * 100;
        if (finalscore < 0) finalscore = 0;
        if (finalscore >= pass_percent) {
            result = "You Passed!";
        } else {
            result = "You Failed";
        }
        $("div#cont").html("<h2>" + result + "</h2><div>Your score: " + finalscore + "%</div>");
    }
	    $("div#image-frame").on("click", "img", function() {


        var ans = $(this).index("img");
        console.log("index:" + ans);
        score += currentQuestion.checkAnswer(ans);
        showNextQuestion();
    });

    $("div#image-frame").on("click", "i.start-btn", function() {
        $("div#timer").show();
        $("#title").hide();
        createQuestions();
        showNextQuestion();
    });

	$("div#image-frame > i").addClass("start-btn");
	
	describe("Creating a question", function() {
		it("should have 4 questions per question", function() {
			expect(createQuestions()).toEqual(4);
		});
		it("should show next question", function() {
			expect(showNextQuestion()).toBe(true);
		});
		
		it("should end when question no exceeds total no of questions", function() {
			question_no = no_questions;
			expect(showNextQuestion()).toBe(false);
		});
	});
	
});
