#!/usr/bin/python
from PIL import Image, ImageFilter
import os, sys

path = "./"
dirs = os.listdir( path )

def resize():
    for item in dirs:
        if os.path.isfile(path+item):
            im = Image.open(path+item)
            f, e = os.path.splitext(path+item)
            imResize = im.resize((500,500), Image.ANTIALIAS)
            #imResize = imResize.filter(ImageFilter.BLUR)
            imResize.save(f + ' resized.jpg', 'JPEG', quality=90)

resize()