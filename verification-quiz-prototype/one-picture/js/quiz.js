$(document).ready(function() {


    // change these ones
    var no_of_questions = 2;
    var pass_percent = 80;



    var images;
    var currentImage;
    var score = 0;
    var question_no = -1;

    // create array of images
    function createQuestions() {
        // for (i = 0; i < no_of_questions; i++) {
        //     var filename = "img/";
        //
        // }
        images = [];
        images.push(new QuizImage("img/hs/1.jpg", true, 4));
        images.push(new QuizImage("img/duds/1.jpg", false, 4));
    }

    function QuizImage(filename, isHouseSparrow, time) {
        this.filename = filename;
        this.active = false;
        this.time = time * 1000;

        this.isHouseSparrow = isHouseSparrow;

        this.display = function() {
            this.active = true;
            $("div#image-frame").html('<img src="' + this.filename + '">');
            setTimeout(function () {
                if (currentImage.active) {
                    currentImage.active = false;
                    showNextQuestion();
                }
            }, this.time);
        };
        this.checkAnswer = function(input) {
            this.active = false;
            $("div#image-frame").html("");
            if (input == this.isHouseSparrow) score++;
            showNextQuestion();
        };
    }

    function displayQuestionNo() {
        $("p#question-number").html("Question " + (question_no+1) + " of " + no_of_questions + ":");
    }

    function showNextQuestion() {
        question_no++;
        if (question_no >= no_of_questions) {
            endQuiz();
            return;
        }
        currentImage = images[question_no];
        currentImage.display();
        displayQuestionNo();
    }

    function endQuiz() {
        var finalscore = (score / no_of_questions) * 100;
        if (finalscore >= pass_percent) {
            result = "You Passed! :)";
        } else {
            result = "You fail :(";
        }
        $("div#quiz").html(result);
    }

    $("div#button-wrapper").on("click", "input#yes, input#no", function() {
        var input = $(this).val();
        input = input === "Yes";
        currentImage.checkAnswer(input);
    });


    $("input#start").on("click", function() {
        createQuestions();
        showNextQuestion();
        $("div#button-wrapper").append('<input type="button" id="yes" value="Yes"><input type="button" id="no" value="No">');
    });

});
