# CSL's House Sparrow Conservation Project with RSPB

This project was developed for RSPB in collaboration with Institute of Biodiversity and Animal Health to spread
awareness and motivate people to help expand the house sparrow conservation effort.

## Links to access the game
* [Browser](https://apps.housesparrowscience.com/data/gameStatic/)
* [Android]()

## Getting Started

This game uses the Unity Engine so to make contributions to the game, download and install Unity3d.

Documentation and installation guide can be found [here](http://130.209.251.77/RSPBSparrows/game/blob/master/docs/Documentation.md).

Release notes can be found [here](http://130.209.251.77/RSPBSparrows/game/blob/master/docs/release-notes.md).

## Technologies Used
* [Unity3d](https://unity3d.com/)
* [NodeJS](https://nodejs.org/en/)
* [PassportJS](http://www.passportjs.org/)
* [MySQL](https://www.mysql.com/)

## Tests

There are no automated testing for the game as we are using Unity and it is difficult to unit test such systems as they are inherently highly coupled.

We used User Acceptance Tests to find bugs and glitches. The documentation for that can be found [here](/docs/UAT.md).

We also created the first prototype of the Sparrow spotting quiz, the initial tests were created using [Jasmine](https://jasmine.github.io/).
The test suite can be ran by just opening the SpecRunner file in /verification-quiz-prototype/four-pictures/jasmine. As this was a collaborative project for RSBP with two teams, our partner team (CSI)
implemented some other features and created more elaborate tests for the quiz using node which can be found in their repository.

## Authors

* Iain Cattermole
* Archit Gupta
* Noel Rajan
* Thomas French Whitehouse

## License

Licensed by GPL3 [here](/LICENSE.txt)
