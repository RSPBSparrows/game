using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickRandomSprite : MonoBehaviour {

	public Sprite[] sprites = {};
	SpriteRenderer m_SpriteRenderer;

	// Use this for initialization
	void Start () {
		m_SpriteRenderer = GetComponent<SpriteRenderer>();
        m_SpriteRenderer.sprite = sprites[Random.Range(0,sprites.Length)];
	}
}
