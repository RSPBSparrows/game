using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleMoveToDestroy : MonoBehaviour {

    public float xSpeed;
    public float ySpeed;

    public float xLimit;
    public float yLimit;

    // Destroy the object when moved passed given position
    void Update() {
        transform.position += new Vector3(xSpeed, ySpeed, 0);
        if ((xSpeed > 0 && transform.position.x > xLimit) || (xSpeed < 0 && transform.position.x < xLimit)) {
            Destroy(gameObject);
        }
        if ((ySpeed > 0 && transform.position.y > yLimit) || (ySpeed < 0 && transform.position.y < yLimit)) {
            Destroy(gameObject);
        }
    }
}
