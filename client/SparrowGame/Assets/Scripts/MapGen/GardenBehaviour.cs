﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * The transform of an object with this script component
 * should be at the center of the first tile at the top right of the garden.
 */
public class GardenBehaviour : MonoBehaviour {

    public int rows = 6;
    public int columns = 6;

    //Size of each tile in unity units (this will likely always be = 1)
    private int tile_size = 1;

    private int min_rows = 6;
    private int min_columns = 6;

    private int max_rows = 8;
    private int max_columns = 14;

    //Rows available for placing items (the others are fences/birdbox slots)
    private int usable_rows, usable_columns;
    private int birdbox_slots = 4;

    // Garden expands to the bottom left,
    // so this array represents each layer of the garden from right to left, top to bottom [<- columns, rows v]
    private GameObject[,,] garden;

    //Garden should track both ground type (grass, long grass, concrete, etc) as well as placed items on this tile.
    private enum LAYERS {GROUND, ITEMS}

    public GameObject fence;
    public GameObject grass;

    private DataControllerBehaviour dataController;
    private InventoryBehaviour inventory;

    // Use this for initialization
	void Start () {
        inventory = GameObject.FindWithTag("Inventory").GetComponent<InventoryBehaviour>();
        dataController = GameObject.FindWithTag("DataController").GetComponent<DataControllerBehaviour>();
        dataController.loadGarden(this);
	}


    public void loadMap(GardenData data){
        // if a map is found load it
        if (data != null) {
            columns = data.columns;
            rows = data.rows;
            // create garden of saved size
            garden = new GameObject[columns, rows, LAYERS.GetValues(typeof(LAYERS)).Length];
            usable_rows = rows-2;
            usable_columns = columns-2;

            // add ground
            for (int i = 0; i < data.ground.Length; i++) {
                PlaceableItemData itemData = data.ground[i];
                GameObject itemInstance = Instantiate(Resources.Load("Prefabs/Items/" + itemData.prefab, typeof(GameObject))) as GameObject;
                PlaceableItem behaviour = itemInstance.GetComponent<PlaceableItem>();
                // restore the items state
                behaviour.restoreState(itemData.data);
                behaviour.tilePos = itemData.tilePos;
                // add item to garden
                garden[(int)behaviour.tilePos.x, (int)behaviour.tilePos.y, (int)LAYERS.GROUND] = itemInstance;
                // set world position of item
                itemInstance.transform.position = tileToPosition(behaviour.tilePos);
                itemInstance.transform.parent = transform;
            }

            // add items
            for (int i = 0; i < data.items.Length; i++) {
                PlaceableItemData itemData = data.items[i];
                GameObject itemInstance = Instantiate(Resources.Load("Prefabs/Items/" + itemData.prefab, typeof(GameObject))) as GameObject;
                PlaceableItem behaviour = itemInstance.GetComponent<PlaceableItem>();
                // restore the items state
                behaviour.restoreState(itemData.data);
                behaviour.tilePos = itemData.tilePos;
                // add item to garden
                garden[(int)behaviour.tilePos.x, (int)behaviour.tilePos.y, (int)LAYERS.ITEMS] = itemInstance;
                // set world position of item
                itemInstance.transform.position = tileToPosition(behaviour.tilePos);
                itemInstance.transform.parent = transform;
            }

        } else {
            // generate a new map
            garden = new GameObject[columns, rows, LAYERS.GetValues(typeof(LAYERS)).Length];
            usable_rows = rows-2;
            usable_columns = columns-2;
            addFences();
            addGrass();
        }
    }

    public void saveMap() {
        List<PlaceableItemData> ground = new List<PlaceableItemData>();
        List<PlaceableItemData> items = new List<PlaceableItemData>();
        GameObject obj;
        PlaceableItem item;

        for (int col = 0; col < garden.GetLength(0); col++) {
            for(int row = 0; row < garden.GetLength(1); row++){
                // save any ground object
                obj = garden[col,row,(int)LAYERS.GROUND];
                if (obj != null) {
                    item = obj.GetComponent<PlaceableItem>();
                    if (item != null) {
                        ground.Add(item.save());
                    }
                }
                // save any item in garden
                obj = garden[col,row,(int)LAYERS.ITEMS];
                if (obj != null) {
                    item = obj.GetComponent<PlaceableItem>();
                    if (item != null) {
                        items.Add(item.save());
                    }
                }
            }
        }

        // package all data into Serializable object and give to DataController
        GardenData data = new GardenData(this, ground, items);
        dataController.saveGarden(data);
    }

    /* Remove everything from a layer of garden, default ITEMS */
    void clearGarden(int layer=(int)LAYERS.ITEMS){
        for(int col = 0; col < garden.GetLength(0); col++){
            for(int row = 0; row < garden.GetLength(1); row++){
                Destroy(garden[col,row,layer]);
            }
        }
    }

    // Resizes a garden
    public void setSize(int new_cols, int new_rows){
        if (new_cols > max_columns) new_cols = max_columns;
        if (new_rows > max_rows) new_rows = max_rows;

        if(new_rows == rows && new_cols == columns){
            return;
        }

        // Return hedges to inventory
        int num_hedges = GameObject.FindGameObjectsWithTag("Hedge").Length;
        if (num_hedges > 0) {
            inventory.addItems(Resources.Load("Prefabs/Items/Hedge", typeof(GameObject)) as GameObject, num_hedges);

        }


        GameObject[,,] new_garden = new GameObject[new_cols, new_rows, LAYERS.GetValues(typeof(LAYERS)).Length];

        //Copy placed items into new garden, removing boundary fences
        for(int col=0; col < Mathf.Min(new_garden.GetLength(0), garden.GetLength(0)); col++) {
            for(int row=0; row < Mathf.Min(new_garden.GetLength(1), garden.GetLength(1)); row++) {
                for(int layer = 0; layer < new_garden.GetLength(2); layer ++){
                    //Delete boundary fences
                    if(layer == (int)LAYERS.ITEMS && tileInBorder(new Vector2(col,row)) && garden[col,row,layer] != null){
                        if(garden[col,row,layer].tag == "Fence" || garden[col,row,layer].tag == "Hedge"){
                            setItemAtTile(null, new Vector2(col,row), save: false);
                        }
                    }

                    //Copy tiles to new garden
                    new_garden[col,row,layer] = garden[col,row,layer];
                }
            }
        }

        rows = new_rows;
        columns = new_cols;
        garden = new_garden;
        usable_rows = rows-2;
        usable_columns = columns-2;

        //Regenerate fences
        addFences();

        //Add grass to unused ground tiles
        addGrass();

        saveMap();
    }

    void refreshGarden(int layer=(int)LAYERS.ITEMS){
        for(int col = 0; col < garden.GetLength(0); col++){
            for(int row = 0; row < garden.GetLength(1); row++){
                if(garden[col,row,layer] != null){
                    garden[col,row,layer].GetComponent<PlaceableItem>().refreshState();
                }
            }
        }
    }

    void refreshNeighbours(Vector2 tilePos, int layer=(int)LAYERS.ITEMS){
        if(!tileInGarden(tilePos)){
            return;
        }

        Vector2[] vectors = new Vector2[]{
            tilePos - Vector2.up,
            tilePos - Vector2.right,
            tilePos - Vector2.down,
            tilePos - Vector2.left
        };

        //TODO get this working
    }

    /* Create fences around perimeter of garden */
    private void addFences(){

        for(int col=0; col< garden.GetLength(0); col++){
            for(int row=0; row < garden.GetLength(1); row++){
                if(row==0 && col > 0 && col < birdbox_slots){
                    //Create birdbox slots
                } else if(row > 0 && row < garden.GetLength(1)-1){
                    if(col==0 || col==garden.GetLength(0)-1){
                        GameObject tmp = Instantiate(fence, tileToPosition(new Vector2(col, row)), Quaternion.identity, transform) as GameObject;
                        garden[col,row, (int)LAYERS.ITEMS] = tmp;
                    }
                } else {
                    GameObject tmp = Instantiate(fence, tileToPosition(new Vector2(col,row)), Quaternion.identity, transform) as GameObject;
                    garden[col,row, (int)LAYERS.ITEMS] = tmp;
                }

            }
        }
    }

    /* Place grass on ground layer */
    private void addGrass(){
        for(int col=1; col < garden.GetLength(0) - 1; col++){
            for(int row=1; row < garden.GetLength(1) - 1; row++){
                if(getItemAtTile(new Vector2(col,row), (int)LAYERS.GROUND) == null){
                    GameObject tmp = Instantiate(grass, tileToPosition(new Vector2(col,row)), Quaternion.identity, transform) as GameObject;
                    garden[col,row, (int)LAYERS.GROUND] = tmp;
                }

            }
        }
    }

    // Converts from tiles to unity space (not guaranteed to be on the map)
    public Vector2 tileToPosition(Vector2 tile){

        Vector2 pos = transform.position;
        pos.x -= tile_size * (int)tile.x;
        pos.y -= tile_size * (int)tile.y;

        return pos;
    }

    // Converts from unity space to tiles (not guaranteed to be on the map)
    public Vector2 positionToTile(Vector2 position){
        //Produces a positive delta vector if position is in the garden quadrant
        Vector2 delta = (Vector2)transform.position - position;
        //Offset of map in unity units
        Vector2 mapOffset = (Vector2)transform.position
            - new Vector2(Mathf.Floor(transform.position.x),Mathf.Floor(transform.position.y));

        float xpos = Mathf.Round(delta.x/tile_size)+mapOffset.x;
        float ypos = Mathf.Round(delta.y/tile_size)+mapOffset.y;

        return new Vector2(xpos,ypos);
    }

    //Snaps a given position to a tile grid i.e, t2p(p2t(pos))
    public Vector2 snapPositionToGrid(Vector2 position){
        Vector2 pos = position;
        pos = new Vector2(Mathf.Round(pos.x / tile_size), Mathf.Round(pos.y / tile_size));
        return pos;
    }

    public Vector2 clampToMap(Vector2 position){
        return new Vector2(Mathf.Clamp(position.x, 0, columns), Mathf.Clamp(position.y, 0, rows));
    }

    // if the tile is in the garden, including border
    public bool tileInGarden(Vector2 tile){
        return tile.x >= 0 && tile.y >= 0 && tile.x < columns && tile.y < rows;
    }

    // if the tile is in the garden, excluding border
    public bool tileInUsableArea(Vector2 tile){
        return tile.x > 0 && tile.y > 0 && tile.x < columns-1 && tile.y < rows-1;
    }

    public bool tileInBorder(Vector2 tile){
        return tile.y == 0 || tile.x == 0 || tile.y == rows-1 || tile.x == columns-1;
    }

    public bool tileInBirdboxArea(Vector2 tile){
        return tile.y == 0 && tile.x <= birdbox_slots;
    }

    public GameObject getItemAtTile(Vector2 tile, int layer=(int)LAYERS.ITEMS){

        // dont accept tiles outside the map area
        if(!tileInGarden(tile)){
            return null;
        }
        return garden[(int)tile.x, (int)tile.y, layer];
    }

    /* Adds or replaces an item at the given tile position */
    public void setItemAtTile(GameObject item, Vector2 tile, int layer=(int)LAYERS.ITEMS, bool save=true){
        if(!tileInGarden(tile)){
           return;
        }

        // remove item if already there
        if(garden[(int)tile.x, (int)tile.y, layer] != null){
            Destroy(garden[(int)tile.x, (int)tile.y, layer]);
        }

        if (item == null){
            garden[(int)tile.x, (int)tile.y, layer] = null;
        } else {
            GameObject instance = Instantiate(item, tileToPosition(tile), Quaternion.identity, transform) as GameObject;
            garden[(int)tile.x, (int)tile.y, layer] = instance;
        }

        refreshGarden();

        if (save) {
            saveMap();
        }
    }

    public GameObject getItemAtPosition(Vector2 position){
        return getItemAtTile(positionToTile(position));
    }

    public int getRows(){
        return rows;
    }

    public int getColumns(){
        return columns;
    }

    public int getWidth(){
        return columns * tile_size;
    }

    public int getHeight(){
        return rows * tile_size;
    }

    public int getMaxRows() {
        return max_rows;
    }

    public int getMaxColumns() {
        return max_columns;
    }

    public int getMinRows() {
        return min_rows;
    }

    public int getMinColumns() {
        return min_columns;
    }

    public Vector2 getNE(){
        return transform.position;
    }

    public Vector2 getNW(){
        return (Vector2)transform.position - new Vector2(getWidth(),0);
    }

    public Vector2 getSW(){
        return (Vector2)transform.position - new Vector2(getWidth(),getHeight());
    }

    public Vector2 getSE(){
        return (Vector2)transform.position - new Vector2(0, getHeight());
    }
}
