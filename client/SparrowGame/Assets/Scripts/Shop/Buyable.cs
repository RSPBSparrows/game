using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Buyable : MonoBehaviour {

    // Shop meta data
    public string productName;
    public string description;
    public int price;
    public Sprite icon;

    public abstract int getPrice();

    public abstract string getName();

    public abstract string getDescription();

    public abstract bool buy();
}
