using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdboxUpgrade : ShopUpgrade {

    // name & description to be shown at minimum & max levels
    public string starting_name;
    public string starting_desc;
    public string maxed_name;
    public string maxed_desc;
    public int price_increase;

    private BirdboxBehaviour birdbox;
    private int min_level;
    private int max_level;

    /* Return the correct price based on the level of upgrade to offer */
    public override int getPrice() {
        checkInit();
        return price + birdbox.getLevel() * price_increase;
    }

    /* Return the correct name based on the level of upgrade to offer */
    public override string getName() {
        checkInit();

        if (!starting_name.Equals("") && birdbox.getLevel() == min_level) {
            return starting_name;
        }

        if (!maxed_name.Equals("") && birdbox.getLevel() == max_level) {
            return maxed_name;
        }

        return productName;
    }

    /* Return the correct description based on the level of upgrade to offer */
    public override string getDescription() {
        checkInit();

        if (!starting_desc.Equals("") && birdbox.getLevel() == min_level) {
            return starting_desc;
        }

        if (!maxed_desc.Equals("") && birdbox.getLevel() == max_level) {
            return maxed_desc;
        }

        return description;
    }

    /* Check if fully upgraded */
    public override bool isMaxed() {
        return birdbox.getLevel() == birdbox.getMaxLevel();
    }

    /* Increase birdbox level by one */
    public override void applyUpgrade() {
        checkInit();
        birdbox.setLevel(birdbox.getLevel()+1);
    }

    /* Check if birdbox has been initialised */
    private void checkInit() {
        if (birdbox) return;
        birdbox = GameObject.FindWithTag("Birdbox").GetComponent<BirdboxBehaviour>();
        max_level = birdbox.getMaxLevel();
        min_level = birdbox.getMinLevel();
    }
}
