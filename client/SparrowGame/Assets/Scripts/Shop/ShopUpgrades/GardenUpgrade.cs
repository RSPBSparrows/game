using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GardenUpgrade : ShopUpgrade {

    public string maxed_name;
    public string maxed_desc;
    public int price_increase;

    private int current_price;
    private GardenBehaviour garden;

    /* Return the correct price based on the level of upgrade to offer */
    public override int getPrice() {
        checkInit();
        // base price + (number of times upgraded * increase per upgrade)
        return price + (garden.getColumns() - garden.getMinColumns()) * price_increase;
    }

    /* Increase garden size by one */
    public override void applyUpgrade() {
        checkInit();
        garden.setSize(garden.getColumns() + 1, garden.getRows() + 1);
    }

    /* Return the correct name based on the level of upgrade to offer */
    public override string getName() {
        checkInit();

        if (!maxed_name.Equals("") && isMaxed()) {
            return maxed_name;
        }

        return productName;
    }

    /* Return the correct description based on the level of upgrade to offer */
    public override string getDescription() {
        checkInit();

        if (!maxed_desc.Equals("") && isMaxed()) {
            return maxed_desc;
        }

        return description;
    }

    /* Check if fully upgraded */
    public override bool isMaxed() {
        return garden.getColumns() == garden.getMaxColumns();
    }

    /* Check the garden has been initialised */
    private void checkInit() {
        if (garden) return;
        garden = GameObject.FindWithTag("Garden").GetComponent<GardenBehaviour>();
    }

}
