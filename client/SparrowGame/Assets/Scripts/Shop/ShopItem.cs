﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopItem : Buyable {

	public GameObject item;

	private InventoryBehaviour inventory;

	public override string getName() {
		return productName;
	}

	public override string getDescription() {
		return description;
	}

	public override int getPrice() {
		return price;
	}

	/* Add item to inventory */
	public override bool buy() {
		if (!inventory) {
			inventory = GameObject.FindWithTag("Inventory").GetComponent<InventoryBehaviour>();
		}
		inventory.addItem(item);
		return true;
	}

	public override int GetHashCode() {
		return productName.GetHashCode();
	}

	public bool Equals(Object obj) {
		ShopItem itemObj = obj as ShopItem;
		if (itemObj == null) return false;
		return productName.Equals(itemObj.productName);
	}
}
