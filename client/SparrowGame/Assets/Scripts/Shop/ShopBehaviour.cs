﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopBehaviour : MonoBehaviour {

	private Balance balance;

	public GameObject[] items;
	public GameObject[] upgrades;

	void Awake() {
		balance = GameObject.FindWithTag("Balance").GetComponent<Balance>();
		print(balance);
	}

	/* Attempt to buy something from the Shop
	   returns true if successful else false */
	public bool purchase(Buyable product) {
		if (balance.canAfford(product.getPrice()) && product.buy()) {
			balance.decrease(product.getPrice());
			return true;
		}
		return false;
	}

	public int getBalance() {
		return balance.getBalance();
	}



}
