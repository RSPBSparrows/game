﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Allows ShopItems to be stored in a Dictionary and compared */
public class ShopItemEqualityComparer : IEqualityComparer<ShopItem> {

	IEqualityComparer<ShopItem> Members;

	public int GetHashCode(ShopItem item) {
		return item.getName().GetHashCode();
	}

	public bool Equals(ShopItem x, ShopItem y) {
		return x.getName().Equals(y.getName());
	}

}
