using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ShopUpgrade : Buyable {

    // can only buy if not fully upgraded
    public override bool buy() {
        if (!isMaxed()) {
            applyUpgrade();
            return true;
        }
        return false;
    }

    public abstract bool isMaxed();
    public override abstract int getPrice();
    public override abstract string getName();
    public override abstract string getDescription();
    public abstract void applyUpgrade();
}
