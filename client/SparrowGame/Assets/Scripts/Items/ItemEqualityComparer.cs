﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemEqualityComparer : IEqualityComparer<ShopItem> {

	IEqualityComparer<ShopItem> Members;

	public int GetHashCode(ShopItem item) {
		return item.getName().GetHashCode();
	}

	public bool Equals(ShopItem x, ShopItem y) {
		return x.getName().Equals(y.getName());
	}

}
