﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.EventSystems;

public class GrassBehaviour : PlaceableItem {

	private string GRASS_WARNING = "Long grass provides cover for predators to sneak up on your sparrows. Keep it under control to keep your sparrows safe!";
	private static bool warned = false;

	public int COIN_VALUE;

	public Sprite longGrassSprite;
	private enum GrassLength {SHORT, LONG}

	public Sprite[] grassVariantSprite;
	private enum GrassVariant { N, NE, E, SE, S, SW, W, NW, C, ALL, NS, EW}

	private Balance bal;
	private GameObject gardenObj;
	private BirdboxBehaviour birdbox;
	private NotificationsBehaviour notifs;

	private int frame;
	private int nextGrowth;

	private GrassLength length;
	private GrassVariant variant;

	private SpriteRenderer sr;

	public GameObject catPrefab;

	private System.Random rnd;

	public bool isLong() {
		return length == GrassLength.LONG;
	}

	void Awake() {
		sr = GetComponent<SpriteRenderer>();
		rnd = new System.Random(GetInstanceID());

		bal = GameObject.FindWithTag("Balance").GetComponent<Balance>();
		gardenObj = GameObject.FindWithTag("Garden");
		birdbox = GameObject.FindWithTag("Birdbox").GetComponent<BirdboxBehaviour>();
		notifs = GameObject.FindWithTag("Notifications").GetComponent<NotificationsBehaviour>();

		// frames until grass grows
		nextGrowth = rnd.Next(2000, 7000);

		variant = GrassVariant.C;
		length = GrassLength.SHORT;

		Vector3 pos = gameObject.transform.position;
		// for inspector debugging
		gameObject.name = "Active-Grass " + ((int) pos.x).ToString() + "," + ((int) pos.y).ToString();

	}

	void Update() {
		// make grass clickable if its long
		if (length == GrassLength.LONG) {
			GetComponent<BoxCollider2D>().enabled = true;
		} else {
			GetComponent<BoxCollider2D>().enabled = false;
		}

		GardenBehaviour garden = gardenObj.GetComponent<GardenBehaviour>();

		if (frame > nextGrowth && length == GrassLength.SHORT) {
			length = GrassLength.LONG;
			if (birdbox.getLevel() == 1 && !warned) {
				warned = true;
				notifs.addNotification(GRASS_WARNING, 1);
			}
		}

		if (garden.getItemAtTile(tilePos)) {
			length = GrassLength.SHORT;
			frame = 0;
			nextGrowth = rnd.Next(2000, 7000);
		}


		// spawn cat only if have birdbox
		if (birdbox.getLevel() > 0) {
			if (length == GrassLength.LONG && frame % 100 == 0 && !GameObject.FindWithTag("Predator")) {
				if (rnd.Next(1,100) == 1) {
					GameObject cat = Instantiate(catPrefab) as GameObject;
					cat.GetComponent<CatBehaviour>().setHidingSpot(this.gameObject);
					Vector3 pos = transform.position;
					cat.transform.position = new Vector3(pos.x, pos.y + 0.3f, 0);
				}
			}
		}


		ChangeVariant();
		ChangeSprite();
		frame++;
	}


	void OnMouseUp() {
		// make sure not clicking through ui
		if(EventSystem.current.IsPointerOverGameObject() || !clicked) return;

		// cut the grass
		if (length == GrassLength.LONG) {
			length = GrassLength.SHORT;
			bal.increase(COIN_VALUE);
			frame = 0;
			nextGrowth = rnd.Next(2000, 7000);
		}
	}

	void ChangeVariant() {
		// default basic grass
		variant = GrassVariant.C;

		Vector3 pos = transform.position;
		// check if house is directly above
		GardenBehaviour garden = gardenObj.GetComponent<GardenBehaviour>();
		if ((int) pos.y >= garden.getNE().y - 1 && (int) pos.x > garden.getNW().x + 1) {
			variant = GrassVariant.N;
		}

		// check tile directly above for objects that require overlap
		GameObject groundObj = garden.getItemAtTile(tilePos + new Vector2(0, -1), 0);
		if (groundObj && groundObj.tag == "Grass" && groundObj.GetComponent<GrassBehaviour>().isLong()) {
			variant = GrassVariant.N;
		}
	}

	/* Choose sprite based on the variant */
	void ChangeSprite() {
		if (length == GrassLength.LONG) {
			sr.sprite = longGrassSprite;
		} else {
			sr.sprite = grassVariantSprite[(int) variant];
		}
	}

	/* Save state to a Serializable object */
	public override PlaceableItemData save() {
		GrassData data = new GrassData(this);
		string json = JsonUtility.ToJson(data);
		return new PlaceableItemData(this, json);
	}

	/* Restore state from json of GrassData */
	public override void restoreState(string json) {
		GrassData data = JsonUtility.FromJson<GrassData>(json);
		length = data.isLong ? GrassLength.LONG : GrassLength.SHORT;
	}

}


[Serializable]
public class GrassData {
	public bool isLong;

	public GrassData(GrassBehaviour grass) {
		isLong = grass.isLong();
	}
}
