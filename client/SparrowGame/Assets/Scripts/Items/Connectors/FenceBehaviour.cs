﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class FenceBehaviour : ConnectorItemBehaviour{

    public BoxCollider2D up;
    public BoxCollider2D left;
    public BoxCollider2D down;
    public BoxCollider2D right;

    //Get the connector state for a given neighbouring item
    protected override int getConnectorStateForNeighbour(GameObject item){
        if(item == null || item.tag != "Fence"){
            //Connected state
            return 0;
        }

        //Unconnected state
        return 1;
    }

    /* Enable colliders for sparrows to interact with based on the connections */
    protected override void chooseSprite(bool propagate){
        base.chooseSprite(propagate);

        // Enable north collider
        GameObject item = garden.getItemAtTile(vectors[0]);
        if(getConnectorStateForNeighbour(item)==1){
            up.enabled = true;
        }else{
            up.enabled = false;
        }

        // Enable east collider
        item = garden.getItemAtTile(vectors[1]);
        if(getConnectorStateForNeighbour(item)==1){
            right.enabled = true;
        }else{
            right.enabled = false;
        }

        // Enable south collider
        item = garden.getItemAtTile(vectors[2]);
        if(getConnectorStateForNeighbour(item)==1){
            down.enabled = true;
        }else{
            down.enabled = false;
        }

        // Enable west collider
        item = garden.getItemAtTile(vectors[3]);
        if(getConnectorStateForNeighbour(item)==1){
            left.enabled = true;
        }else{
            left.enabled = false;
        }
    }
}
