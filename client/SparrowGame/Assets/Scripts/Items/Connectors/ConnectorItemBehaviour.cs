﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/*
 * Creates items that connect with each other. Each side can have N states.
 * 
 * The state of a connector item is conceptually represented as a 4 digit N-ary number, where each column represents a side (NESW).
 * 
 * For example, a connector with binary side states (i.e, connected/not connected) is represented as a binary number.
 * '1011' would represent the state of that object where N,S and W sides are connected, and E is not.
 *
 * A connector with ternary side states is represented as a ternary number. 
 * '2001' would represent an object with N=state 2, W=state 1, E==S==state 0. 
 *
 * As such, the sprites array must be populated according to this scheme (counting up in n-ary).
 * 
 * For binary this looks like:
 * 0    1  2   3  4  5   6   7    8  9   10  11   12  13   14   15
 * NONE, W, S, SW, E, EW, SE, SEW, N, NW, NS, NSW, NE, NEW, NES, NESW
 *
 *
 * */
public abstract class ConnectorItemBehaviour : PlaceableItem {

    public Sprite[] sprites;
    public int statesPerSide=2;

    bool justCreated=true;
    public int sprite;
    protected Vector2[] vectors;

    public override void Start(){
        base.Start();

        vectors = new Vector2[]{
            tilePos - Vector2.up,
            tilePos - Vector2.right,
            tilePos - Vector2.down,
            tilePos - Vector2.left
        };
    }

    public override void refreshState(){
        base.refreshState();

        vectors = new Vector2[]{
            tilePos - Vector2.up,
            tilePos - Vector2.right,
            tilePos - Vector2.down,
            tilePos - Vector2.left
        };

        chooseSprite(justCreated);
    }

    /*
     * propagate: force neighbouring fences to refresh state
     */
    protected virtual void chooseSprite(bool propagate){
        garden = GameObject.FindWithTag("Garden").GetComponent<GardenBehaviour>();
        justCreated = false;
        sprite = 0;

        // Look in all cardinal directions
        // (everything is backwards in the garden array)


        for(int i=0; i< vectors.Length; i++){
            GameObject item = garden.getItemAtTile(vectors[i]);

            if(item != null){
                //Perform a magic trick (~'-')>━ﾟ.*・｡ﾟ
                sprite += (int)(Mathf.Min((float)getConnectorStateForNeighbour(item), (float)statesPerSide-1) * Mathf.Pow(statesPerSide,(3-i)));

                if(propagate){
                    ConnectorItemBehaviour c = item.GetComponent<ConnectorItemBehaviour>();
                    if(c != null){
                        c.refreshState();
                    }
                }
            }
        }
        setSprite(sprites[Mathf.Min(sprites.Length-1,sprite)]);
    }

    public void setSprite(Sprite sprite){
        SpriteRenderer renderer = GetComponent<SpriteRenderer>();
        renderer.sprite = sprite;
    }

    //Get the connector state for a given neighbouring item
    protected abstract int getConnectorStateForNeighbour(GameObject item);
}
