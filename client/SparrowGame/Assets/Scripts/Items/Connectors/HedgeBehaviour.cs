﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class HedgeBehaviour : ConnectorItemBehaviour{

    //Get the connector state for a given neighbouring item
    protected override int getConnectorStateForNeighbour(GameObject item){
        if(item==null) return 0;
        
        if(item.tag == "Hedge"){
            //Connected to a hedge state
            return 1;
        }
        
        /*if(item.tag == "Fence"){
            //Connected to a fence state
            return 2;
        }*/

        //Unconnected state
        return 0;
    }

    // can plant hedges only on a fence
    public override bool isValidPlacement(Vector2 tile) {
        garden = GameObject.FindWithTag("Garden").GetComponent<GardenBehaviour>();
        if (!garden.tileInGarden(tile)) return false;

        GameObject item = garden.getItemAtTile(tile);

        return item != null && item.tag == "Fence";
    }

}
