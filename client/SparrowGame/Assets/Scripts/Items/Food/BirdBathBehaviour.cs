using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BirdBathBehaviour : Food {

    private SpriteRenderer sprite;

    public Sprite fullSprite;
    public Sprite emptySprite;
    private AudioSource audio;

    private int max_supply = 75;


    void Awake() {
        audio = GetComponent<AudioSource>();
        hunger_value = 0.001;
        // if not already initialised
        if (!sprite) {
            supply = 50;
        }
    }

    // Refill birdbath to max
    public void fill() {
        audio.Play();
        supply = max_supply;
        sprite = GetComponent<SpriteRenderer>();
        sprite.sprite = fullSprite;
    }

    // Set empty sprite
    public override void onEmpty() {
        sprite = GetComponent<SpriteRenderer>();
        sprite.sprite = emptySprite;
    }

    public override void onClick() {
        if (supply <= 0) {
            fill();
        }
    }


}
