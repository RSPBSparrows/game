using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeadowBehaviour : Food {

    public Sprite fullSprite;
    public Sprite emptySprite;

    private int max_supply = 300;
    private SpriteRenderer sprite;

    private int growthFrame = 0;
    private int next_growth = 0;

    void Awake() {
        // if not already initialised
        if (!sprite && supply == 0) {
            supply = max_supply;
            sprite = GetComponent<SpriteRenderer>();
        }
    }
    // Growing sprites
    public override void onUpdate() {
        if (supply == 0 && growthFrame > next_growth) {
            supply = max_supply;
            sprite.sprite = fullSprite;
        }
        growthFrame++;
    }
    // Set empty sprites
    public override void onEmpty() {
        sprite = GetComponent<SpriteRenderer>();
        sprite.sprite = emptySprite;
        next_growth = growthFrame + 10000;
    }

}
