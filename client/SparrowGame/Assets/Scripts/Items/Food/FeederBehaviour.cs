using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FeederBehaviour : Food {

    public GameObject seedsObject;

    public Sprite empty;
    public Sprite full;
    private AudioSource audio;

    private SpriteRenderer sprite;
    private int max_supply = 75;

    void Awake() {
      audio = GetComponent<AudioSource>();
        // if not already initialised
        if (!sprite) {
            supply = 50;
        }
    }

    // Refill to max
    public void fill() {
        sprite = GetComponent<SpriteRenderer>();
        supply = max_supply;
        sprite.sprite = full;
        audio.Play();
    }
    // Set empty sprite
    public override void onEmpty() {
        sprite = GetComponent<SpriteRenderer>();
        sprite.sprite = empty;
    }

}
