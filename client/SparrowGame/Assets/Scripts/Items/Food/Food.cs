﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public abstract class Food : PlaceableItem {

	protected int supply;
	protected double hunger_value = 0.01;

	/* Sparrow can Eat food, reducing supply
	   and recieving the hunger to restore
	   from the Eat */
	public double Eat() {
		if (supply > 0) {
			supply--;
		}
		if (supply <= 0) {
			onEmpty();
		}
		return hunger_value;
	}

	public int getSupply() {
		return supply;
	}

	public void setSupply(int sup) {
		supply = sup;
		if (supply <= 0) {
			onEmpty();
		}
	}

	public bool hasFood() {
		return supply > 0;
	}

	/* Behaviour when supply runs out */
	public abstract void onEmpty();

	/* Create Serializable object containing item data */
	public override PlaceableItemData save() {
		FoodData data = new FoodData(this);
		string json = JsonUtility.ToJson(data);
		return new PlaceableItemData(this, json);
	}

	/* Restore supply state from json of FoodData */
	public override void restoreState(string json) {
		FoodData data = JsonUtility.FromJson<FoodData>(json);
		setSupply(data.supply);
	}
}

[Serializable]
public class FoodData {
	public int supply;

	public FoodData(Food food) {
		this.supply = food.getSupply();
	}
}
