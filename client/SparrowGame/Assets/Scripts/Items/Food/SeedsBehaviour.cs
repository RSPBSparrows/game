﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeedsBehaviour : Food {

	void Awake() {
		this.supply = 50;
	}

	public override void onEmpty() {
		Destroy(this.gameObject);
	}

	/* Seeds can be placed on ground or empty birdfeeder */
	public override bool isValidPlacement(Vector2 tile) {
		garden = GameObject.FindWithTag("Garden").GetComponent<GardenBehaviour>();

		if (!garden.tileInUsableArea(tile)) return false;
		GameObject item = garden.getItemAtTile(tile);
		if (item != null && item.GetComponent<FeederBehaviour>()) {
			// birdfeeder has no food so can be filled
			return !item.GetComponent<FeederBehaviour>().hasFood();
		}
		// not already got an item
		return item == null;
	}

	/* Refill birdfeeder or place on ground */
	public override void place(GameObject newItem, Vector2 tile) {
		garden = GameObject.FindWithTag("Garden").GetComponent<GardenBehaviour>();
		GameObject item = garden.getItemAtTile(tile);
		if (item != null && item.GetComponent<FeederBehaviour>()) {
			FeederBehaviour feeder = item.GetComponent<FeederBehaviour>();
			feeder.fill();
			garden.saveMap();
			return;
		}

		garden.setItemAtTile(newItem, tile);
	}
}
