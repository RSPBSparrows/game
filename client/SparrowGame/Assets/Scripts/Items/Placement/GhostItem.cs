using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Hovering item with no behaviour used for placement purposes
public class GhostItem : MonoBehaviour {

	// to be set by shop
	public GameObject item;
	private string state;
	bool moving = true;
	GardenBehaviour garden;
	Vector2 tile;

	private PlaceableItem placeItem;

	private NotificationsBehaviour notifs;
	private Notification moveNotif;

	private static string MOVE_MESSAGE = "Hold down on an item to move it about in your garden!";
	private static int moveMessageShown = 0;

	public void Start(){
		garden = GameObject.FindWithTag("Garden").GetComponent<GardenBehaviour>();
		notifs = GameObject.FindWithTag("Notifications").GetComponent<NotificationsBehaviour>();
	}

	public void setItem(GameObject item) {
		setItem(item, "");
	}
	// Set the item to move
	public void setItem(GameObject item, string state){
		this.state = state;
		this.item = item;
		placeItem = item.GetComponent<PlaceableItem>();
		Sprite sprite = item.GetComponent<SpriteRenderer>().sprite;
		// set clickable box size to sprite size
		GetComponent<BoxCollider2D>().size = sprite.bounds.size;
		GetComponent<SpriteRenderer>().sprite = sprite;
	}

	public void Update(){
		// If the item is moving set tiling
		if(moving){
			GameObject.FindWithTag("MainCamera").GetComponent<CameraBehaviour>().panEnabled = false;
			tile = garden.positionToTile(Camera.main.ScreenToWorldPoint(Input.mousePosition));
			transform.position = garden.snapPositionToGrid(Camera.main.ScreenToWorldPoint(Input.mousePosition));
		}
		// If item is at valid position indicate green otherwise red
		if(placeItem.isValidPlacement(tile)){
			GetComponent<Renderer>().material.color = Color.white;

			for(int i = 0; i < transform.GetChildCount(); i++){
				transform.GetChild(i).GetComponent<Renderer>().material.color = Color.green;
			}
		}else{
			GetComponent<Renderer>().material.color = Color.red;

			for(int i = 0; i < transform.GetChildCount(); i++){
				transform.GetChild(i).GetComponent<Renderer>().material.color = Color.red;
			}
		}

		// when click is released then try and place item at location
		// if failed then add item back to inventory
		if (Input.GetMouseButtonUp (0)){
			moving = false;
			GameObject.FindWithTag("MainCamera").GetComponent<CameraBehaviour>().panEnabled = true;

			if(placeItem.isValidPlacement(tile)){
				placeItem.place(item, tile);
				if (state != "") {
					garden.getItemAtTile(tile).GetComponent<PlaceableItem>().restoreState(this.state);
				}
				if (moveMessageShown < 3) {
					Notification newMoveNotif = notifs.addNotification(MOVE_MESSAGE, 2, 10);
					if (moveNotif != newMoveNotif) {
						moveMessageShown++;
						moveNotif = newMoveNotif;
					}
				}
			}else{
				InventoryBehaviour inventory = GameObject.FindWithTag("Inventory").GetComponent<InventoryBehaviour>();
				inventory.addItem(item);
			}

			Destroy(gameObject);

		}
	}

	public void OnMouseDown(){
		moving = true;
	}

}
