﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Concept for placing items within the garden */
public class SimplePlaceBehaviour : MonoBehaviour {

    private GardenBehaviour garden;
    public GameObject placeObject;

	// Use this for initialization
	void Start () {
		garden = GameObject.Find("Garden").GetComponent<GardenBehaviour>();
	}

	// Update is called once per frame
	void Update () {
        Vector3 mouse = Input.mousePosition;
        transform.position = Camera.main.ScreenToWorldPoint(mouse);

        if (Input.GetMouseButton(0)){
            Vector3 position = Camera.main.ScreenToWorldPoint(mouse);
            garden.setItemAtTile(placeObject,garden.positionToTile(position));
        }

        if (Input.GetMouseButton(1)){
            Vector3 position = Camera.main.ScreenToWorldPoint(mouse);
            garden.setItemAtTile(null,garden.positionToTile(position));
        }
	}
}
