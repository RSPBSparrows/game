﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlaceableItem : Item {

    // to save prefab name
    public string prefabName;

    protected GardenBehaviour garden;
    public Vector2 tilePos;

    // hold to drag
    protected bool clicked = false;
    private int clickHeld = 0;


	// Use this for initialization
	public virtual void Start () {
        garden = GameObject.FindWithTag("Garden").GetComponent<GardenBehaviour>();
        refreshState();
	}

    void Update() {
        // only move items bought from the shop
        if (GetComponent<ShopItem>() && clicked ) {
            print(clickHeld);
            if (Input.GetMouseButton(0) && !GameObject.FindWithTag("GhostItem")) {
                clickHeld++;
                if (clickHeld > 50) {
                    GameObject ghostitem = Resources.Load("Prefabs/GhostItem", typeof(GameObject)) as GameObject;
                    GameObject prefab = Resources.Load("Prefabs/Items/" + prefabName, typeof(GameObject)) as GameObject;
                    GameObject gi = Instantiate(ghostitem, Camera.main.ScreenToWorldPoint(Input.mousePosition), Quaternion.identity, null);
                    string state = this.save().data;
                    gi.GetComponent<GhostItem>().setItem(prefab, state);
                    garden.setItemAtTile(null, tilePos);
                }
            } else {
                clickHeld = 0;
                clicked = false;
            }
        }
        onUpdate();
    }

    // extra update behaviour can be added
    public virtual void onUpdate() { }

    // Pseudo-observer pattern thingy
    // Basically an item should update
    // its own state whenever this is called (used for fences)
	public virtual void refreshState () {
		garden = GameObject.FindWithTag("Garden").GetComponent<GardenBehaviour>();
        tilePos = garden.positionToTile(transform.position);
	}

    public override bool inventoryBehaviour(InventoryBehaviour inventory){
        GameObject gi = Instantiate(inventory.ghostitem, Camera.main.ScreenToWorldPoint(Input.mousePosition), Quaternion.identity, null);
        gi.GetComponent<GhostItem>().setItem(gameObject);
        return true;
    }

    // basic placement rule for items: within fence and not already occupied
    public virtual bool isValidPlacement(Vector2 tile) {
        garden = GameObject.FindWithTag("Garden").GetComponent<GardenBehaviour>();
        return garden.tileInUsableArea(tile) && garden.getItemAtTile(tile) == null;
    }
    // can be called after isValidPlacement
    public virtual void place(GameObject newItem, Vector2 tile) {
        garden = GameObject.FindWithTag("Garden").GetComponent<GardenBehaviour>();
        garden.setItemAtTile(newItem, tile);
    }

    // recieve click and begin picking up sequence
    public void OnMouseDown() {
        if(EventSystem.current.IsPointerOverGameObject()) return;
        clicked = true;
        print(gameObject.name);
        onClick();
    }

    public void OnMouseExit() {
        clicked = false;
        clickHeld = 0;
    }
    // method for items to add their own extra onClick functionality
    public virtual void onClick() { }


    // get serialisable state data
    public virtual PlaceableItemData save() {
        return new PlaceableItemData(this);
    }

    // if not overriden then nothing to restore and
    // will recieve empty string
    public virtual void restoreState(string json) { }

}
