﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Item : MonoBehaviour {
    //Some Item properties   
   //Performs this items' inventory behaviour
   public abstract bool inventoryBehaviour(InventoryBehaviour inventory); 
}
