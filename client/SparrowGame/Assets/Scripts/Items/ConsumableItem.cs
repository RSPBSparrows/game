﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConsumableItem : Item {

	public virtual void Start () {
        refreshState();
	}

    // Pseudo-observer pattern thingy
    // Basically an item should update
    // its own state whenever this is called (used for fences)
	public virtual void refreshState () {
	}

    public override bool inventoryBehaviour(InventoryBehaviour inventory){
        //Do nothing
        return false;
    }
}
