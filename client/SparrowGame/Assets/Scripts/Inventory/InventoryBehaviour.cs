﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class InventoryBehaviour : MonoBehaviour {
	private Dictionary<GameObject, int> inventory;
    public GameObject ghostitem;
    public GameObject[] testinv = new GameObject[10];

	private DataControllerBehaviour dataController;
	//Initialization
	void Start() {
		inventory = new Dictionary<GameObject, int>();
		//Load inventory data
		dataController = GameObject.FindWithTag("DataController").GetComponent<DataControllerBehaviour>();
        foreach(GameObject t in testinv){
            addItem(t);
        }
		dataController.loadInventory(this);
    }

	/* Restore inventory state from Serializable object */
	public void loadItems(InventoryData data) {
		if (data != null) {
			for (int i = 0; i < data.items.Length; i++) {
				InventoryItemData item = data.items[i];
				GameObject prefab = Resources.Load("Prefabs/Items/" + item.prefab, typeof(GameObject)) as GameObject;
				inventory.Add(prefab, item.quantity);
			}
		}
	}

	/* Create Serializable object containing invenory items and quantities */
	public void saveItems() {
		List<InventoryItemData> itemDatas = new List<InventoryItemData>();
		foreach (KeyValuePair<GameObject, int> item in inventory) {
			itemDatas.Add(new InventoryItemData(item.Key, item.Value));
		}
		InventoryData data = new InventoryData(itemDatas);
		dataController.saveInventory(data);
	}


	public int getQuantity(GameObject item) {
		if (inventory.ContainsKey(item)) {
			return inventory[item];
		}
		return 0;
	}

	public bool addItem(GameObject item) {
		return addItems(item, 1);
	}

	public bool addItems(GameObject item, int quantity) {
        if (item==null) return false;

		if (inventory.ContainsKey(item)) {
			// increase quantity of item
			inventory[item] += quantity;
		} else {
			// add item to Dictionary
			inventory.Add(item, quantity);
		}
		saveItems();
        return true;
	}

    //Performs the item behaviour (when inventory is clicked, for example)
    public bool activateItem(GameObject item){
        Item i = item.GetComponent<Item>();
        print(item);
        if(removeItem(item)){
            return i.inventoryBehaviour(GetComponent<InventoryBehaviour>());
        }

        return false;
    }

    //Remove an item from the dictionary (without instantiating)
	public bool removeItem(GameObject item) {
		if (inventory.ContainsKey(item)) {
			if (inventory[item] > 1) {
				// if have more than 1 decrease by 1
				inventory[item]--;
			} else {
				// have only 1 so now have 0 and remove the key
				inventory.Remove(item);
			}
			saveItems();
			return true;
		}
		return false;
	}

	public List<GameObject> getAllItems() {
		return new List<GameObject>(inventory.Keys);
	}
}
