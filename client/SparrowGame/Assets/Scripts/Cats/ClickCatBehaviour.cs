using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ClickCatBehaviour: MonoBehaviour {

	CatBehaviour cat;
	Balance bal;

	const int COIN_VALUE = 15;

	void Awake() {
		cat = GetComponent < CatBehaviour > ();
		bal = GameObject.FindWithTag("Balance").GetComponent < Balance > ();
	}

	// if cat is tapped then shoo it away and reward coins
	void OnMouseUp() {
		if (EventSystem.current.IsPointerOverGameObject()) return;
		if (cat.shoo()) {
			bal.increase(COIN_VALUE);
		}
	}
}
