using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CatBehaviour: MonoBehaviour {

	private GameObject hidingSpot;
	private GameObject target;
	private Vector3 pos;
	public GameObject smokebomb; // To make the sparrow poof away

	private GardenBehaviour garden;

	private NotificationsBehaviour notifs;
	private Notification catNotification;
	private const string CATMESSAGE = "A cat has snuck into your garden! Shoo it away!";

	private int waitFrames;
	//Speed of cat
	private float maxSpeed = 0.03f;
	private float xSpeed;
	private float ySpeed;
	//Boundaries for cat
	private int xMax;
	private int xMin;
	private int yMax;
	private int yMin;

	private static System.Random rnd;

	public string colour;

	// current state
	public CatState state;
	public enum CatState {
		Hidden,
		Hunting,
		Moving,
		Leaving,
		Eating
	};

	private Animator animator;
	private bool finished;
	//Initialization
	void Start() {
		rnd = new System.Random();
		animator = gameObject.GetComponent < Animator > ();

		waitFrames = rnd.Next(100, 200);
		finished = false;
		state = CatState.Hidden;
	}

	void Awake() {
		garden = GameObject.FindWithTag("Garden").GetComponent < GardenBehaviour > ();
		notifs = GameObject.FindWithTag("Notifications").GetComponent < NotificationsBehaviour > ();
		catNotification = notifs.addNotification(CATMESSAGE, 0);
	}

	void Update() {
		pos = this.gameObject.transform.position;

		switch (state) {
			case CatState.Hidden:
			hidden();
			break;
			case CatState.Hunting:
			hunting();
			break;
			case CatState.Moving:
			moving();
			break;
			case CatState.Leaving:
			leaving();
			break;
			case CatState.Eating:
			eating();
			break;
			default:
			break;
		}
		changeAnimation();
	}

	/* sparrows should not react when cat is hiding in grass or leaving garden */
	public bool isHidden() {
		return state == CatState.Hidden || state == CatState.Leaving;
	}

	/* put cat into leaving state */
	public bool shoo() {
		if (finished) return false;
		finished = true;
		state = CatState.Leaving;
		notifs.deleteNotification(catNotification);
		return true;
	}

	public void setHidingSpot(GameObject h) {
		hidingSpot = h;
	}

	/* perform hidden state action */
	void hidden() {
		// check that hiding spot is long grass
		if (hidingSpot == null || !hidingSpot.GetComponent < GrassBehaviour > ().isLong()) {
			hidingSpot = chooseHidingSpot();

			// run out of hiding spots
			if (hidingSpot == null) {
				state = CatState.Leaving;
				return;
			}
			state = CatState.Moving;
			return;
		}

		xSpeed = 0;
		ySpeed = 0;

		if (waitFrames > 0)
		waitFrames--;
		else
		state = CatState.Hunting;
	}

	/* Perform hunting behaviour */
	void hunting() {
		// Don't chase safe sparrows
		if (target != null && target.GetComponent < SparrowBehaviour > ().isSafe()) {
			target = null;
		}

		// Find nearest sparrow if not already got a target
		if (target == null) {
			GameObject[] possible = findSparrowTargets();

			// no vulnerable sparrows
			if (possible.Length < 1) {
				state = CatState.Moving;
				return;

			} else if (possible.Length < 2)
				target = possible[0];
			else {
				target = Helpers.FindNearestFromArray(pos.x, pos.y, possible).gameObject;

			}
		}

		Vector3 direction = Helpers.FromToDirection(pos, target.transform.position);

		xSpeed = direction.x * maxSpeed;
		ySpeed = direction.y * maxSpeed;

		// If cat catches up with sparrow, then eat
		if (Helpers.DistanceBetween(pos.x, pos.y, target.transform.position.x, target.transform.position.y) < 0.5) {
			SparrowBehaviour sparrow = target.GetComponent < SparrowBehaviour > ();
			sparrow.kill("Oh no! " + sparrow.getName() + " has been eaten by a cat!");
			waitFrames = 50;
			state = CatState.Eating;
		}

		if (pos.x + xSpeed >= garden.getNE().x - 0.5f || pos.x + xSpeed <= garden.getSW().x + 1.5f) xSpeed = 0;
		if (pos.y + ySpeed >= garden.getNE().y - 0.5f || pos.y + ySpeed <= garden.getSW().y + 2f) ySpeed = 0;

		this.transform.position += new Vector3(xSpeed, ySpeed, 0);
	}

	// Wait while eating and then hide
	void eating() {
		if (waitFrames > 0) {
			waitFrames--;
		} else {
			state = CatState.Moving;
		}
	}

	// Move to hiding spot
	void moving() {
		// check hiding spot is still valid
		if (hidingSpot == null || !hidingSpot.GetComponent < GrassBehaviour > ().isLong()) {
			hidingSpot = chooseHidingSpot();
			if (hidingSpot == null) {
				state = CatState.Leaving;
				notifs.deleteNotification(catNotification);
			}
			return;
		}

		// move in direction of hiding spot
		Vector3 direction = Helpers.FromToDirection(pos, hidingSpot.transform.position);
		xSpeed = direction.x * maxSpeed;
		ySpeed = direction.y * maxSpeed;
		transform.position += new Vector3(xSpeed, ySpeed, 0);

		// check if reached hiding spot
		if (Helpers.DistanceBetween(pos.x, pos.y, hidingSpot.transform.position.x, hidingSpot.transform.position.y) < 0.5) {
			state = CatState.Hidden;
			waitFrames = 1000;
			transform.position = hidingSpot.transform.position;
		}

	}

	/* Move right until at the garden boundary */
	void leaving() {
		xSpeed = 0.05f;
		this.transform.position += new Vector3(xSpeed, 0, 0);

		if (pos.x + xSpeed > garden.getNE().x) {
			Instantiate(smokebomb, this.transform.position, transform.rotation = Quaternion.identity);
			Destroy(this.gameObject);
		}
	}

	// Animation according to state of cat
	void changeAnimation() {
		if (state == CatState.Hidden) {
			animator.Play(colour + "-cat-hidden");
		} else {
			if (xSpeed > 0 && xSpeed > Math.Abs(ySpeed)) {
				// east
				animator.Play(colour + "-cat-east");
			} else if (xSpeed < 0 && -xSpeed > Math.Abs(ySpeed)) {
				// west
				animator.Play(colour + "-cat-west");
			} else if (ySpeed > 0 && ySpeed >= Math.Abs(xSpeed)) {
				// north
				animator.Play(colour + "-cat-north");
			} else if (ySpeed < 0 && -ySpeed >= Math.Abs(xSpeed)) {
				// south
				animator.Play(colour + "-cat-south");
			}
		}
	}

	/* Find a long grass object. If none return null */
	GameObject chooseHidingSpot() {
		GameObject[] allGrass = GameObject.FindGameObjectsWithTag("Grass");
		foreach(GameObject grass in allGrass) {
			if (grass.GetComponent < GrassBehaviour > ().isLong()) {
				return grass;
			}
		}
		return null;
	}

	/* Get array of sparrows that are vulnerable */
	GameObject[] findSparrowTargets() {
		List < GameObject > targets = new List < GameObject > ();
		GameObject[] sparrows = GameObject.FindGameObjectsWithTag("Sparrow");
		SparrowBehaviour sparrow;
		for (int i = 0; i < sparrows.Length; i++) {
			sparrow = sparrows[i].GetComponent < SparrowBehaviour > ();
			if (!sparrow.isSafe() && objectInGarden(sparrow.gameObject)) {
				targets.Add(sparrow.gameObject);
			}
		}
		return targets.ToArray();
	}

	/* Check if an object is within the bounds of the garden */
	bool objectInGarden(GameObject sparrow) {
		Vector3 sparrowPos = sparrow.transform.position;
		return (sparrowPos.x >= garden.getSW().x - 0.5 && sparrowPos.y <= garden.getNE().x + 0.5) &&
		(sparrowPos.y >= garden.getSW().y - 0.5 && sparrowPos.y <= garden.getNE().y + 0.5);
	}

}
