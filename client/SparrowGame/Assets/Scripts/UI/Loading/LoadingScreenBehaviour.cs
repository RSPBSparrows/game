﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingScreenBehaviour : MonoBehaviour {

	private GameObject panel;
	private Slider slider;

	public static LoadingScreenBehaviour instance;

	void Awake() {
		// keep canvas alive through scene change
		DontDestroyOnLoad(gameObject);
		panel = transform.Find("GreenBackground").gameObject;
		slider = panel.transform.Find("LoadingPanel").Find("LoadingBar").GetComponent<Slider>();
		instance = this;
	}

	public void toggle(bool open) {
		print(open);
		panel.SetActive(open);
	}

	public void setBarPercent(float value) {
		slider.value = value;
	}
}
