﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingDots : MonoBehaviour {

	public string message;

	private string[] dots = new string[] {"", " .", " . .", " . . ."};
	private int current = 0;
	private Text txt;
	private int frame = 1;

	// Use this for initialization
	void Start () {
		txt = GetComponent<Text>();
		txt.text = message + dots[current];
	}

	void Update () {
		// move forward one step every 15 frames
		if (frame % 15 == 0) {
			current = (current + 1) % dots.Length;
			txt.text = message + dots[current];
		}

		frame++;
	}
}
