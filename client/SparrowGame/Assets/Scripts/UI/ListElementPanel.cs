using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public abstract class ListElementPanel : MonoBehaviour, IPointerDownHandler{
    public GameObject onClickSoundPrefab;

    public void OnPointerDown(PointerEventData eventData) {
        if(onClickSoundPrefab != null){
            Instantiate(onClickSoundPrefab);
        }

        click(eventData);
    }

    /* background change to colour and back to original after delay */
    public void colorFeedback(Image bg, Color color, Color original, float time=0.1f) {
        bg.color = color;
        StartCoroutine(changeBack(bg, original, time));
    }

    /* Show green background */
    public void colorSucess(Image bg, Color original, float time=0.1f) {
        colorFeedback(bg, new Color32(130,221,85,255), original, time);
    }

    /* Show red background */
    public void colorFail(Image bg, Color original, float time=0.1f) {
        colorFeedback(bg, new Color32(226,54,54,255), original, time);
    }

    /* Panels have an onClick behaviour */
    public abstract void click(PointerEventData eventData);

    /* Revert back to normal colour after delay */
    IEnumerator changeBack(Image bg, Color color, float delay) {
        yield return new WaitForSeconds(delay);
        bg.color = color;
        yield return null;
    }
}
