using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Manages panel such that they don't overlap
// Makes sure only one panel is opened at a time
public class PanelManager: MonoBehaviour {

	public GameObject clickOffPanel;
    public GameObject switchPanelAudioPrefab;
	private CloseAllPanels closeAll;
	private int[] panelTime;

	void Awake() {
		closeAll = clickOffPanel.GetComponent < CloseAllPanels > ();
		panelTime = new int[closeAll.panels.Length];
	}

	//Closes all panels and only let the panel the user intends remain open
	void Update() {
		int open = 0;
		for (int i = 0; i < closeAll.panels.Length; i++) {
			ListPanel panel = closeAll.panels[i].GetComponent < ListPanel > ();
			if ((panel != null && panel.isOpen) || (panel == null && closeAll.panels[i].activeSelf)) {
				panelTime[i]++;
				open++;
			} else {
				panelTime[i] = 0;
			}
		}

		// close oldest panel if 2 are open
		if (open > 0) {
			clickOffPanel.SetActive(true);
			GameObject.FindWithTag("MainCamera").GetComponent<CameraBehaviour>().panEnabled = false;
			if (open > 1) {
				GameObject panel = closeAll.panels[Helpers.IndexOfMax(panelTime)];
                if(switchPanelAudioPrefab != null){
                    Instantiate(switchPanelAudioPrefab);
                }
				if (panel.GetComponent<ListPanel>() != null) {
					panel.GetComponent<ListPanel>().close();
				} else {
					panel.SetActive(false);
				}
			} else {
				if (closeAll.panels[Helpers.IndexOfMax(panelTime)].name == "InventoryPanel") {
					GameObject.FindWithTag("MainCamera").GetComponent<CameraBehaviour>().panEnabled = true;
				}
			}

		} else {
			clickOffPanel.SetActive(false);
			GameObject.FindWithTag("MainCamera").GetComponent<CameraBehaviour>().panEnabled = true;
		}

	}
}
