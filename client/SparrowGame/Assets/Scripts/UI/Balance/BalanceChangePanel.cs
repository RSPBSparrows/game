using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class BalanceChangePanel : MonoBehaviour {
    private AudioSource audio;
    public void Awake(){
      audio = GetComponent<AudioSource>();
    }
    /* Change the text and colour of child panel to show coin
       gain or loss */
    public void setChange(int coins) {
        Color green;
        ColorUtility.TryParseHtmlString("#2db533", out green);
        Color red;
        ColorUtility.TryParseHtmlString("#9d250a", out red);

        Text text = transform.GetChild(0).GetComponent<Text>();
        if (coins > 0) {
            text.text = "+ " + Math.Abs(coins).ToString();
            text.color = green;
            audio.Play();
        } else {
            text.text = "- " + Math.Abs(coins).ToString();
            text.color = red;
        }
    }
}
