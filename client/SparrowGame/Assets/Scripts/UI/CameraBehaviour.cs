using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// All movements for the Camera
// Zooming and panning
public class CameraBehaviour: MonoBehaviour {
	private static readonly float panSpeed = 0.00333f;
	private static readonly float zoomSpeed = 1f;

	public static float xMin = 0f, yMin = 0f, xMax = 27f, yMax = 14f;

	public static readonly float[] BoundsX = new float[] {
		xMin,
		xMax
	};
	public static readonly float[] BoundsY = new float[] {
		yMin,
		yMax
	};
	public static readonly float[] ZoomBounds = new float[] {
		2f, 6f
	};

	private Camera cam;
	private Vector3 panOrigin;
	private bool panActive = false;
	public bool panEnabled = true;

	private bool zoomActive = false;
	public bool zoomEnabled = true;
	private Vector2[] lastZoomPositions; // Touch mode only
	private float width, height;


	void Awake() {
		cam = GetComponent < Camera > ();
		UpdateDimensions();
		ClampToBounds();
	}

	void Update() {

		// Getting input to decide what the user wanrs to do
		if (Input.touchSupported && Application.platform != RuntimePlatform.WebGLPlayer) {
			if (panEnabled) {
				TouchPan();
			}
			if (zoomEnabled) {
				PinchZoom();
			}

			ClampToBounds();
		} else {
			if (panEnabled) {
				MousePan();
			}

			if (zoomEnabled) {
				MouseZoom();
			}

			ClampToBounds();
		}
	}
	// How to handle panning when the game recieves touch input
	void TouchPan() {
		panActive = false;
		Touch touch = Input.GetTouch(0);
		if (touch.phase == TouchPhase.Began) {
			panOrigin = touch.position;
		}

		if (Input.touchCount != 1) return;
		panActive = true;

		// set position of camera
		Vector3 delta = (Input.mousePosition - panOrigin) * GetComponent < Camera > ().orthographicSize * panSpeed;
		transform.position -= delta;
		panOrigin = Input.mousePosition;
	}
	// How to handle panning when the game recieves mouse input
	void MousePan() {
		panActive = false;
		if (Input.GetMouseButtonDown(0)) {
			panOrigin = Input.mousePosition;
			return;
		}

		if (!Input.GetMouseButton(0)) return;
		panActive = true;

		// set position of camera
		Vector3 delta = (Input.mousePosition - panOrigin) * GetComponent < Camera > ().orthographicSize * panSpeed;
		transform.position -= delta;
		panOrigin = Input.mousePosition;
	}
	// How to handle zooming when the game recieves touch input
	void PinchZoom() {
		zoomActive = false;
		panEnabled = true;
		if (Input.touchCount < 2) return;
		panEnabled = false;
		zoomActive = true;

		// Store both touches.
		Touch touchZero = Input.GetTouch(0);
		Touch touchOne = Input.GetTouch(1);

		Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
		Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;
		// set position of camera
		float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
		float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;
		float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

		if (GetComponent < Camera > ().orthographic) {
			float size = GetComponent < Camera > ().orthographicSize + deltaMagnitudeDiff * 0.01f; //speed
			size = Mathf.Clamp(size, ZoomBounds[0], ZoomBounds[1]);
			GetComponent < Camera > ().orthographicSize = size;
		}

		UpdateDimensions();
	}
	// How to handle zoom when the game recieves mouse input
	void MouseZoom() {
		float scroll = Input.GetAxis("Mouse ScrollWheel");
		zoomActive = false;
		if (Mathf.Abs(scroll) < 0.0001) return;
		zoomActive = true;
		if (GetComponent < Camera > ().orthographic) {
			float size = GetComponent < Camera > ().orthographicSize + -scroll * zoomSpeed;
			size = Mathf.Clamp(size, ZoomBounds[0], ZoomBounds[1]);
			GetComponent < Camera > ().orthographicSize = size;
		}
		UpdateDimensions();
	}

	void UpdateDimensions() {
		Camera cam = GetComponent < Camera > ();
		height = 2f * cam.orthographicSize;
		width = height * cam.aspect;
	}

	// Don't let it go out of bounds
	void ClampToBounds() {
		Vector3 pos = transform.position;
		pos.x = Mathf.Clamp(transform.position.x, BoundsX[0] + width / 2, BoundsX[1] - width / 2);
		pos.y = Mathf.Clamp(transform.position.y, BoundsY[0] + height / 2, BoundsY[1] - height / 2);
		transform.position = pos;
	}
}
