using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SlideFadeOut : MonoBehaviour {

    public float xSpeed;
    public float ySpeed;
    public float fadeSpeed = 1f;
    public int fadeAfterFrames = 0;

    private RectTransform rect;
    private CanvasGroup canvasGroup;
    private int frame = 0;

    void Start() {
        rect = GetComponent<RectTransform>();
        canvasGroup = GetComponent<CanvasGroup>();
    }

    void Update() {
        // move panel
        rect.anchoredPosition += new Vector2(xSpeed, ySpeed);

        // after certain number of frames start to fade out
        if (frame >= fadeAfterFrames) {
            canvasGroup.alpha -= 0.01f * fadeSpeed;
            // destroy after fade out
            if (canvasGroup.alpha <= 0) {
                Destroy(gameObject);
            }
        }

        frame++;
    }
}
