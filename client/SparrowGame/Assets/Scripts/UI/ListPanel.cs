using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ListPanel : MonoBehaviour {

	private List<GameObject> panels;
    public int page_size=4;
    public int current_page=0;
    public float marginLeft=14, marginRight=14, marginTop=50, marginBottom=50, itemYPadding=5;
    public bool isOpen;
    public GameObject leftArrowButtonPrefab, rightArrowButtonPrefab;
    private GameObject leftArrowButton, rightArrowButton;

	void Start () {
        RectTransform rectTransform = GetComponent<RectTransform>();
        panels = new List<GameObject>();
        leftArrowButton = Instantiate(leftArrowButtonPrefab, transform);
        rightArrowButton = Instantiate(rightArrowButtonPrefab, transform);
        rightArrowButton.SetActive(false);
        leftArrowButton.SetActive(false);
        close();
    }

    protected abstract List<GameObject> getPanels();

    //Open and display a certain page
    public void open(int page = 0){
		if (panels != null) {
			foreach(GameObject panel in panels){
				Destroy(panel);
			}
		}

        panels = getPanels();
        alignPanels();

        //Start with all panels disabled
        foreach(GameObject panel in panels){
            panel.SetActive(false);
        }

        
        if(page < 0){
            page = 0;
        }

        //If our inventory is too small for this page, set to the last page we can get to.
        if(getMaxPage() <= page){
            page = getMaxPage();
        }

        //Display this page
        for(int i=page_size*page; i < Mathf.Min(page_size*page+page_size, panels.Count); i++){
            panels[i].SetActive(true);
        }

        //Show the UI and enable interaction
        current_page = page;
        CanvasGroup group = GetComponent<CanvasGroup>();
        
        //Handle pagination buttons if there is more than one page
        if(getMaxPage() > 0 && current_page != getMaxPage()){
            rightArrowButton.SetActive(true);
        } else {
            rightArrowButton.SetActive(false);
        }

        if(current_page > 0){
            leftArrowButton.SetActive(true);
        } else {
            leftArrowButton.SetActive(false);
        }

        group.alpha = 1;
        group.interactable = true;
        group.blocksRaycasts = true;
        isOpen = true;
    }

    //align panels within pages according to margins and padding
    void alignPanels(){
        if(panels.Count == 0){
            return;
        }

        RectTransform rectTransform = GetComponent<RectTransform>();
        RectTransform subpanel;
        Vector2 size = new Vector2(0,0);//rectTransform.sizeDelta;
        float max_width = 0;
        float max_height = 0;

        //Find maximum width and height of subpanels
        for(int i=0; i< panels.Count; i++){
            subpanel = panels[i].GetComponent<RectTransform>();
            size = subpanel.sizeDelta;

            if(size.x > max_width){
                max_width = size.x;
            }

            if(size.y > max_height){
                max_height = size.y;
            }
        }
        
        //Adjust the width and height of the container panel based on the max widths and heights of the subpanel, and the padding.
        GetComponent<RectTransform>().sizeDelta = new Vector2(marginLeft + marginRight + max_width, marginTop+marginBottom+ max_height*page_size + itemYPadding*(page_size-1));

        //Lay the panels out
        for(int i=0; i< panels.Count; i++){
            subpanel = panels[i].GetComponent<RectTransform>();
            size = subpanel.sizeDelta;

            //Update position
            subpanel.anchoredPosition = new Vector2(0, -marginTop - (itemYPadding+size.y)*(i%page_size));
        }

        //Lay out the pagination buttons
        RectTransform leftRect = leftArrowButton.GetComponent<RectTransform>();
        leftRect.anchoredPosition = new Vector2(-50, 0);
        
        RectTransform rightRect = rightArrowButton.GetComponent<RectTransform>();
        rightRect.anchoredPosition = new Vector2(50, 0);
    }

    //Close the panel
    public void close(){
        //Hide UI and turn off interaction
        CanvasGroup group = GetComponent<CanvasGroup>();
        group.alpha = 0;
        group.interactable = false;
        group.blocksRaycasts = false;
        isOpen = false;

        rightArrowButton.SetActive(false);
        leftArrowButton.SetActive(false);

        foreach(GameObject panel in panels){
            panel.SetActive(false);
        }

    }

    public int getMaxPage(){
        return (int)Mathf.Max(Mathf.Floor((panels.Count-1)/page_size), 0);
    }
}
