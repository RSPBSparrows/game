﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ChangePageElementPanel : ListElementPanel {

    public GameObject parentpanel;
    public enum Direction {LEFT,RIGHT};
    public Direction direction = Direction.LEFT;
    ListPanel panel;

	// Use this for initialization
	void Start () {
        if (parentpanel == null){
            parentpanel = transform.parent.gameObject;
        }

        panel = parentpanel.GetComponent<ListPanel>();
        setDirection(direction);
	}

	// Update is called once per frame
    public override void click(PointerEventData eventData){
        if(direction == Direction.LEFT){
            panel.open(panel.current_page-1);
        } else {
            panel.open(panel.current_page+1);
        }
    }

    public void setDirection(Direction direction){
        this.direction = direction;
    }
}
