	using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class InventoryListPanel : ListPanel {

	private InventoryBehaviour inventory;
	public GameObject panelPrefab;
	public GameObject noItem;

	protected override List<GameObject> getPanels(){
		if (!inventory) {
			inventory = GameObject.FindWithTag("Inventory").GetComponent<InventoryBehaviour>();
		}

		List<GameObject> items = inventory.getAllItems();
		if(items.Count == 0){
			noItem.SetActive(true);
			transform.Find("Instruction").gameObject.SetActive(false);
		}
		
		// create panel for each item
		List<GameObject> panels = new List<GameObject>();
		foreach(GameObject item in items){
			noItem.SetActive(false);
			transform.Find("Instruction").gameObject.SetActive(true);
			GameObject panel = Instantiate(panelPrefab, transform);
			panel.name= item.transform.name + " Panel";
			panel.GetComponent<InventoryElementPanel>().setItem(item);
			panel.SetActive(false);
			panels.Add(panel);
		}

		return panels;
	}
}
