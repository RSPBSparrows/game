﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;

public class InventoryElementPanel : ListElementPanel  {

    private GameObject item;
    InventoryBehaviour inventory;
	private Image background;


	void Awake() {
        inventory = GameObject.FindWithTag("Inventory").GetComponent<InventoryBehaviour>();
	}

    /* Update panel with item details */
    public void setItem(GameObject i){
        if (inventory == null || i == null) return;
        item = i;
		transform.GetChild(0).GetComponent<Text>().text = item.transform.name;
		transform.GetChild(1).GetComponent<Text>().text = "x " + inventory.getQuantity(item).ToString();
        transform.Find("Icon").GetComponent<Image>().sprite = item.GetComponent<ShopItem>().icon;
    }

	public override void click(PointerEventData eventData) {
        bool success = inventory.activateItem(item);
        if(success){
            transform.parent.GetComponent<InventoryListPanel>().close();
        }
	}

}
