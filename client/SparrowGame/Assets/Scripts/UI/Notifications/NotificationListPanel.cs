using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NotificationListPanel : ListPanel {

	public GameObject notificationPanel;
	public Sprite[] icons;

	private NotificationsBehaviour notifications;

	void Awake() {
		notifications = GameObject.FindWithTag("Notifications").GetComponent<NotificationsBehaviour>();
	}

	protected override List<GameObject> getPanels() {
		List<Notification> notifs = notifications.getNotifications();

		// create a panel for each notfication
		List<GameObject> panels = new List<GameObject>();
		Notification notif;
		for (int i = 0; i < notifs.Count; i++) {
			notif = notifs[i];
			GameObject panel = Instantiate(notificationPanel, transform) as GameObject;
			panel.SetActive(false);
			panel.name = notif.getMessage();
			NotificationElementPanel behaviour = panel.GetComponent<NotificationElementPanel>();
			behaviour.setNotification(notif, icons[notif.getPriority()]);
			panels.Add(panel);
		}
		return panels;
	}

	public void update() {
		open();
	}

}
