﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;

public class NotificationElementPanel : ListElementPanel  {

	private Notification notification;

	/* Delete notification on click */
	public override void click(PointerEventData eventData) {
		GameObject.FindWithTag("Notifications").GetComponent<NotificationsBehaviour>().deleteNotification(notification);
	}

	/* Show notification icon and message on panel */
	public void setNotification(Notification notif, Sprite icon) {
		notification = notif;
		transform.GetChild(0).GetComponent<Image>().sprite = icon;
		transform.GetChild(1).GetComponent<Text>().text = notif.getMessage();
	}

}
