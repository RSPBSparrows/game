﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class OpenListPanel : MonoBehaviour {

	private Button panelButton;
	public GameObject panel;
    public GameObject openPanelAudioPrefab;
    public GameObject closePanelAudioPrefab;

	// Use this for initialization
	void Start () {
		Button btn = GetComponent<Button>();
		if (btn) {
			btn.onClick.AddListener(TaskOnClick);
		}
	}

	// Button on click behaviour
	void TaskOnClick() {

        ListPanel panelBehaviour = panel.GetComponent<ListPanel>();

        if(panelBehaviour == null){
            return;
        }

		if (panelBehaviour.isOpen){
            if(closePanelAudioPrefab != null){
                Instantiate(closePanelAudioPrefab);
            }

            panelBehaviour.close();
        } else{
            if(openPanelAudioPrefab != null){
                Instantiate(openPanelAudioPrefab);
            }

            panelBehaviour.open(0);
        }
    }

	// GameObject on click behaviour
	void OnMouseDown() {
		if (EventSystem.current.IsPointerOverGameObject()) return;
		TaskOnClick();
	}
}
