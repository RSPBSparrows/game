using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleMute : MonoBehaviour {

    public Sprite mutedSprite;
    public Sprite unmutedSprite;

    bool muted = false;

    /* add listener to button */
    void Start() {
        Button btn = GetComponent<Button>();
        btn.onClick.AddListener(toggle);
    }

    /* toggle game between muted and unmuted */
    void toggle() {
        muted = !muted;

        Image img = GetComponent<Image>();
        if (muted) {
            AudioListener.volume = 0;
            img.sprite = mutedSprite;
        } else {
            AudioListener.volume = 1f;
            img.sprite = unmutedSprite;
        }
    }


}
