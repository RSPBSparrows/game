using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CloseAllPanels: MonoBehaviour, IPointerClickHandler {

	public GameObject[] panels;
    public GameObject closePanelAudioPrefab;

	// If clicked outside any panel, close it.
	public void OnPointerClick(PointerEventData eventData) {
        if(closePanelAudioPrefab){
		    Instantiate(closePanelAudioPrefab);
        }
        closeAll();
	}

	// Close every panel
	public void closeAll() {

		for (int i = 0; i < panels.Length; i++) {
			// handle ListPanels and normal panels separately
			ListPanel panel = panels[i].GetComponent < ListPanel > ();
			if (panel != null) {
				panel.close();
			} else {
				panels[i].SetActive(false);
			}
		}
	}
}
