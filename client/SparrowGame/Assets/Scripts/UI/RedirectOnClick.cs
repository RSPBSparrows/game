using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RedirectOnClick : MonoBehaviour {

    public string url;

    void Start() {
        Button btn = GetComponent<Button>();
        btn.onClick.AddListener(redirect);
    }

    /* open page */
    void redirect() {
        Application.OpenURL(url);
    }
}
