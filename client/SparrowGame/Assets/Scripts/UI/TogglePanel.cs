using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class TogglePanel : MonoBehaviour {

	public GameObject panel;

	void Start () {
		Button btn = GetComponent<Button>();
		if (btn) {
			btn.onClick.AddListener(togglePanel);
		}
	}

	/* Toggle panel showing and hiding */
    void togglePanel() {
        panel.SetActive(!panel.activeSelf);
    }

}
