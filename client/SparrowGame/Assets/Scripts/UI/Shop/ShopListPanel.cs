using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopListPanel : ListPanel {

	public GameObject itemPanel;
	public GameObject upgradePanel;

	private ShopBehaviour shop;

	protected override List<GameObject> getPanels() {

		if (!shop) {
			shop = GameObject.FindWithTag("Shop").GetComponent<ShopBehaviour>();
		}

		// create a panel for each product
		List<GameObject> panels = new List<GameObject>();

		if (shop.items.Length > 0)
			createPanelsIntoList(shop.items, itemPanel, panels);
		if (shop.upgrades.Length > 0)
			createPanelsIntoList(shop.upgrades, upgradePanel, panels);

		return panels;
	}

	/* Create panels with given prefab adding to end of referenced list */
	void createPanelsIntoList(GameObject[] buyables, GameObject panelPrefab, List<GameObject> panels) {
		Buyable product;
		for (int i = 0; i < buyables.Length; i++) {
			product = buyables[i].GetComponent<Buyable>();
			GameObject panel = Instantiate(panelPrefab, transform) as GameObject;
			panel.SetActive(false);
			panel.name = product.getName() + " Panel";
			panel.GetComponent<ShopElementPanel>().setProduct(product);
			panels.Add(panel);
		}
	}


}
