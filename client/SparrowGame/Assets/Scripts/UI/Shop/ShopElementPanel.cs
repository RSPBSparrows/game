﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;

public class ShopElementPanel : ListElementPanel  {

	private ShopBehaviour shop;
	private Buyable product;
	private Image background;
	private Color bgcol;
	private AudioSource audio;

	void Awake() {
		background = GetComponent<Image>();
		bgcol = background.color;
		shop = GameObject.Find("Shop").GetComponent<ShopBehaviour>();
		audio = GetComponent<AudioSource>();
	}

	/* Buy item when clicked */
	public override void click(PointerEventData eventData) {
		if (shop.purchase(product)) {
			audio.Play();
			colorSucess(background, bgcol);
		} else {
			colorFail(background, bgcol);
		}
		updatePanel();
	}

	public void setProduct(Buyable p) {
		product = p;
		gameObject.name = "ShopElementPanel: " + product.getName();
		updatePanel();
	}

	/* Update panel with product details */
	public void updatePanel() {
		transform.Find("Name").GetComponent<Text>().text = product.getName();
		transform.Find("Price").GetComponent<Text>().text = product.getPrice().ToString();
		transform.Find("Description").GetComponent<Text>().text = product.getDescription();
		transform.Find("Icon").GetComponent<Image>().sprite = product.icon;
	}
}
