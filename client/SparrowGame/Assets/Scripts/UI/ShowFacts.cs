
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowFacts: MonoBehaviour {
  // List of facts
  string[] facts = new string[] {
    "House sparrows have thick bills designed for eating seeds and they will readily eat most mixes.",
    "Parents look for high-protein insects to feed their young chicks.",
    "Wildflower plots provide food year-round: insects in the spring and summer, and seeds in the autumn and winter!",
    "Place feeders within a metre of a hedge or bush to provide quick cover when predators are near.",
    "Clean your bird feeder regularly to prevent disease! ",
    "Sunflowers are great for house sparrow gardens!",
    "House sparrows are colonial nesters.",
    "Attract nesting house sparrows by providing multiple nest boxes.",
    "House sparrows often find crevices in older buildings to nest in.",
    "House sparrows lay three to five eggs, which both parents care for.",
    "Keep an eye out for both parents carrying food to their nestlings.",
    "Young house sparrows leave their nest after two weeks, but receive care from their parents for another couple of weeks.",
    "National Nest Box Week is 14-21 February! The prefect time to clean out your nest boxes!",
    "Hedges are very important: they provide safe cover for house sparrows.",
    "Let your hedges grow a little wild—avoid hedge trimming in the breeding season (March – August)!",
    "By reducing the number of times and limiting when hedges are trimmed, you can help provide a place for house sparrows.",
    "The house sparrows’ scientific name is Passer domesticus.",
    "House sparrow numbers have fallen by 70% across the UK and by as much as 90% in Glasgow since the 1970s.",
    "New build houses typically lack the crevices and holes used by house sparrows to nest in, contributing to their decline.",
    "House sparrows tend to stay in a local area, often near where they were hatched and raised.",
    "House sparrows are very social birds, living and nesting in communities.",
    "Colour ringing allows the RSPB to find and identify individuals and look at movement and dispersal patterns.",
    "World Sparrow Day is the 20th March, and has been celebrated since 2010.",
    "Sparrows are small birds, reaching around 4 to 8 inches in length and 0.8 to 1.4 ounces in weight.",
    "One of the reasons why sparrows adapted to live in human settlements is due to a constant supply of food. Sparrows learned to eat 'served food' when people started to build bird feeders.",
    "House sparrows are naturally carnivores, but they changed their eating habits when they learned to live close to people.",
    "Sparrows primarily eat moths and other small insects, but they can also eat seeds, berries and fruit.",
    "House sparrows usually fly at around 24 miles per hour. When needed (in case of danger), they can accelerate to around 31 miles per hour.",
    "House sparrows have been found living and breeding 640m underground in a coalmine, and others have been recorded living their whole lives inside warehouses.",
    "Though a long-established resident of Britain, sparrows are not thought to be native, but spread naturally north from North Africa.",
    "In towns and cities, house sparrows often scavenge for food in garbage containers and congregate outside of restaurants and other eating establishments to feed on leftover food and crumbs.",
    "The house sparrow requires grit to digest the harder items in its diet. Grit can be either stone, or the shells of eggs or snails; oblong and rough grains are preferred.",
    "House sparrows can perform complex tasks to obtain food, such as opening automatic doors to enter supermarkets, and clinging to walls to steal food from balconies!",
    "House sparrows have been observed stealing prey from other birds, including American robins.",
    "On the ground, the house sparrow typically hops rather than walks.",
    "Protecting insect habitats on farms, and planting native plants in cities benefit the house sparrow, as does establishing urban green spaces.",
    "The house sparrow is the state bird of Delhi, India.",
    "House sparrows sleep with their beak tucked under their wing feathers",
  };

  NotificationsBehaviour notifications;
  Notification notif;

  void Start() {
    notifications = GetComponent < NotificationsBehaviour > ();
    StartCoroutine(timeoutNewFact(10));
  }

  // Show facts as notification at random time intervals
  public void showFact() {
    notifications.addNotification(facts[Random.Range(0, facts.Length)], 2, 30);
    StartCoroutine(timeoutNewFact(Random.Range(45, 75)));
  }

  //Timeout delay to show notification
  IEnumerator timeoutNewFact(int delay) {
    yield
    return new WaitForSeconds(delay);
    showFact();
    yield
    return null;
  }

}
