using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;

public class LogoutPanel : ListElementPanel {

    /* Logout on click */
    public override void click(PointerEventData eventData) {
        DataControllerBehaviour dataController = GameObject.FindWithTag("DataController").GetComponent<DataControllerBehaviour>();
        dataController.logout();
    }

}
