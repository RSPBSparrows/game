﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdboxListPanel : ListPanel {

	private BirdboxBehaviour birdbox;
	public GameObject panelPrefab;
	public GameObject nobirds;

	protected override List<GameObject> getPanels() {
		if (!birdbox) {
			birdbox = GameObject.FindWithTag("Birdbox").GetComponent<BirdboxBehaviour>();
		}

		GameObject[] sparrows = birdbox.getAllSparrows();
		if(sparrows.Length == 0){
			nobirds.SetActive(true);
		}

		// create a panel for each sparrow
		List<GameObject> panels = new List<GameObject>();
		SparrowStatus status;
		for (int i = 0; i < sparrows.Length; i++) {
			nobirds.SetActive(false);
			status = sparrows[i].GetComponent<SparrowStatus>();

			GameObject panel = Instantiate(panelPrefab, transform) as GameObject;
			panel.SetActive(false);
			panel.GetComponent<BirdboxElementPanel>().setSparrow(status);
			panel.name = status.getName() + " Panel";

			panels.Add(panel);
		}

		return panels;
	}

}
