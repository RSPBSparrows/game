﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class BirdboxElementPanel : ListElementPanel {

	private SparrowStatus sparrow;

	public void setSparrow(SparrowStatus sparrow) {
		this.sparrow = sparrow;
		this.gameObject.transform.GetChild(1).GetComponent<InputField>().text = sparrow.getName();
	}

	// Update is called once per frame
	void Update () {
		if (!sparrow) return;
		// update stats in panel
		sparrow.setName(this.gameObject.transform.GetChild(1).GetComponent<InputField>().text);
		this.gameObject.transform.GetChild(2).GetComponent<Slider>().value = (float)sparrow.getHealth();
		this.gameObject.transform.GetChild(3).GetComponent<Slider>().value = 1-(float)sparrow.getHunger();
		this.gameObject.transform.GetChild(4).GetComponent<Slider>().value = (float)sparrow.getHappiness();
	}


	public override void click(PointerEventData eventData) {
		// nothing currently
	}
}
