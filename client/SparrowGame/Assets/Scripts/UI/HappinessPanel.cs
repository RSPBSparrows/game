using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HappinessPanel : MonoBehaviour {

    public Sprite[] faces;

    private Text percentText;
    private Image faceImage;

    private BirdboxBehaviour birdbox;
    int frame = 0;

    void Start() {
        faceImage = transform.Find("Face").GetComponent<Image>();
        percentText = transform.Find("HappyPercent").GetComponent<Text>();

        birdbox = GameObject.FindWithTag("Birdbox").GetComponent<BirdboxBehaviour>();
    }

    void Update() {
        // update every 200 frames
        if (frame % 200 == 0) {
            double percent = 0;

            GameObject[] allSparrows = birdbox.getAllSparrows();
            int num_sparrows = allSparrows.Length;
            if (num_sparrows == 0) {
                updatePanels(0);
            } else {
                // find the average percent happiness of sparrows
                foreach (GameObject sparrow in allSparrows) {
                    SparrowBehaviour behaviour = sparrow.GetComponent<SparrowBehaviour>();
                    percent += behaviour.getHappiness();
                }
                percent /= num_sparrows;
                updatePanels(percent);
            }

        }
        frame++;
    }

    /* Show percent happiness on panel with accompanying icon */
    void updatePanels(double percent) {
        if (percent > 1) percent = 1;
        else if (percent < 0) percent = 0;

        percentText.text = (percent*100).ToString("N0") + "%";
        faceImage.sprite = faces[(int)((percent - 0.1)*5)];
    }
}
