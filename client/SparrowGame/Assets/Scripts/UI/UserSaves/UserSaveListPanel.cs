using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserSaveListPanel : ListPanel {

    public GameObject savePanel;

    private DataControllerBehaviour dataController;

    void Awake() {
        dataController = GameObject.FindWithTag("DataController").GetComponent<DataControllerBehaviour>();
    }

    protected override List<GameObject> getPanels() {
        string[] usernames = dataController.getLocalUsers();

        List<GameObject> panels  = new List<GameObject>();

        if (usernames == null) return panels;

        // create panel for each username
        string username;
        for (int i = 0; i < usernames.Length; i++) {
            username = usernames[i];

            GameObject panel = Instantiate(savePanel, transform) as GameObject;
            panel.SetActive(false);
            panel.name = username;

            UserSaveElementPanel behaviour = panel.GetComponent<UserSaveElementPanel>();
            behaviour.setUsername(username);
            panels.Add(panel);
        }
        return panels;
    }

}
