using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;

public class UserSaveElementPanel : ListElementPanel {

    private string username;

    /* Load save with username */
    public override void click(PointerEventData eventData) {
        GameObject.FindWithTag("DataController").GetComponent<DataControllerBehaviour>().setCurrentUser(username, true);
    }

    /* Update panel to show username */
    public void setUsername(string username) {
        this.username = username;
        transform.GetChild(0).GetComponent<Text>().text = username;
    }
}
