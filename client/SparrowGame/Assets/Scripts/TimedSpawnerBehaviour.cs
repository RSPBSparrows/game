
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// A timer for the spawning of gameobjects
public class TimedSpawnerBehaviour: MonoBehaviour {
    public float timer = 120;
    public float resetvalue = 3;
    public int maxInstances = 50;
    public GameObject prefab;

    void Update() {
        if (timer > 0) {
            timer -= Time.deltaTime;
        } else {
            if (resetvalue > 0) {
                timer = resetvalue;
            }

            if(GameObject.FindGameObjectsWithTag(prefab.tag).Length < maxInstances){
                Instantiate(prefab, transform);
            }
        }
    }
}
