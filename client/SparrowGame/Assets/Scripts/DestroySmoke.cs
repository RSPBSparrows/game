﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroySmoke : MonoBehaviour {

	// Destroy the smoke bomb animation
	void FixedUpdate(){
		Destroy(gameObject, 1.0f);
	}
}
