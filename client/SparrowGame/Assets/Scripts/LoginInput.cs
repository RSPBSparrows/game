using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

// Used to get input for the login scene
public class LoginInput: MonoBehaviour {
    EventSystem system;

    void Start() {
        system = EventSystem.current; // EventSystemManager.currentSystem;

    }
    // When "Tab" key is pressed move to a new Selectable panel
    void Update() {
        if (Input.GetKeyDown(KeyCode.Tab)) {
            GameObject current = system.currentSelectedGameObject;
            if (current == null) {
                return;
            }
            Selectable next = current.GetComponent < Selectable > ().FindSelectableOnDown();
            if (next != null) {

                InputField inputfield = next.GetComponent < InputField > ();
                if (inputfield != null)
                inputfield.OnPointerClick(new PointerEventData(system)); //if it's an input field, also set the text caret

                system.SetSelectedGameObject(next.gameObject, new BaseEventData(system));
            } else {
                system.SetSelectedGameObject(Selectable.allSelectables[0].gameObject, new BaseEventData(system));
            }
            //else Debug.Log("next nagivation element not found");

        }
    }
}
