﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Sounds for the game
public class PlaySoundOnce : MonoBehaviour {
	private AudioSource audioSource;

	// Use this for initialization
	void Start () {
		audioSource = GetComponent<AudioSource>();
		audioSource.Play();
	}

	void Update () {
	    if (!audioSource.isPlaying){
            Destroy(gameObject);
        }
    }
}
