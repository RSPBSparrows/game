﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Sounds for the game
public class SparrowSounds : MonoBehaviour {
	public AudioClip chirping;
	public AudioClip background;
	private AudioSource audioSource;

	// Use this for initialization
	void Awake () {
		audioSource = GetComponent<AudioSource>();
		audioSource.clip = chirping;
		audioSource.Play();
	}

	void Update () {
	}
}
