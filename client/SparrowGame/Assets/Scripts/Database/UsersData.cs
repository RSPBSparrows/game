using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

/* Objects that can be serialized to JSON for database and local storage */

[Serializable]
public class UsersData {
    public string[] usernames;

    public UsersData(string[] users, string newUser) {
        usernames = new string[users.Length+1];
        usernames[0] = newUser;
        Debug.Log(usernames.Length);
        for (int i = 1; i < usernames.Length; i++) {
            usernames[i] = users[i-1];
        }
    }

    public UsersData(string newUser) {
        usernames = new string[] {newUser};
    }
}
