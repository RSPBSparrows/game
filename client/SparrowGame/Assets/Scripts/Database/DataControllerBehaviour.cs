using UnityEngine;
using UnityEngine.Networking;
using System;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DataControllerBehaviour : MonoBehaviour {

    public GameObject loginpanel;
    private GameObject errorpanel;
    private InputField usernameInput;
    private InputField passwordInput;
    private Toggle rememberme;

    static string BASE_URL = "https://apps.housesparrowscience.com";
    static string LOGIN_URL = BASE_URL + "/auth/login";
    static string SPARROW_URL = BASE_URL + "/game/sparrows/";
    static string INVENTORY_URL = BASE_URL + "/game/inventory/";
    static string GARDEN_URL = BASE_URL + "/game/garden/";
    static string BALANCE_URL = BASE_URL + "/game/balance/";

    static int COOKIE_EXPIRY_DAYS = 14;

    static string secret_key = "q0927riahbfy0q83ytibahlfao23rha";

    static string currentUser;
    static bool playingLocally = false;

    static Authentication auth;
    static bool stayLoggedIn = false;

    static bool savingBalance;

    // number of times loaded
    private int loaded = 0;

    void Start() {
        // persist to game scene
        DontDestroyOnLoad(transform.gameObject);

        errorpanel = loginpanel.transform.Find("ErrorMessage").gameObject;
        usernameInput = loginpanel.transform.Find("Username").GetComponent<InputField>();
        passwordInput = loginpanel.transform.Find("Password").GetComponent<InputField>();
        rememberme = loginpanel.transform.Find("RememberMe").GetComponent<Toggle>();
        loginpanel.transform.Find("Login").GetComponent<Button>().onClick.AddListener(login);

        // uncomment to delte local data
        // PlayerPrefs.DeleteAll();
        // PlayerPrefs.Save();

        checkPlayerPrefs();
    }

    /* If remember me was clicked previously autofill fields */
    void checkPlayerPrefs() {
        if (PlayerPrefs.HasKey("RememberMe")) {
            print(PlayerPrefs.GetString("LoginTime"));
            DateTime loginTime = DateTime.Parse(PlayerPrefs.GetString("LoginTime"));
            if (DateTime.Now.Subtract(loginTime).Days > COOKIE_EXPIRY_DAYS-1) {
                // login expired so prompt relogin
                usernameInput.text = PlayerPrefs.GetString("uname");
                errorpanel.GetComponent<Text>().text = "Login expired";
                errorpanel.SetActive(true);
                return;
            }

            // activate loading screen
            LoadingScreenBehaviour.instance.setBarPercent(0);
            LoadingScreenBehaviour.instance.toggle(true);
            string token = Encrypt.DecryptString(PlayerPrefs.GetString("RememberMe"), secret_key);
            // load game as remembered user
            auth = new Authentication();
            auth.token = token;
            auth.auth = true;
            setCurrentUser(PlayerPrefs.GetString("uname"));
        }
    }

    /* load the game scene as user: username */
    public void setCurrentUser(string username, bool local=false) {
        LoadingScreenBehaviour.instance.setBarPercent(0);
        LoadingScreenBehaviour.instance.toggle(true);
        currentUser = username;
        playingLocally = local;
        SceneManager.LoadScene("main", LoadSceneMode.Single);
    }

    /* save when exiting or pausing */
    void OnApplicationQuit() {
        // force save balance
        savingBalance = false;
        saveAll();
    }
    void OnApplicationPause() {
        saveAll();
    }

    void finishedLoad() {
        loaded++;
        LoadingScreenBehaviour.instance.setBarPercent(loaded * 0.25f);
        if (loaded == 4) {
            LoadingScreenBehaviour.instance.toggle(false);
        }
    }

    /* save state of everything */
    public void saveAll() {
        // only save if in game scene
        if (SceneManager.GetActiveScene().name == "main") {
            GameObject.FindWithTag("Birdbox").GetComponent<BirdboxBehaviour>().saveSparrows();
            GameObject.FindWithTag("Garden").GetComponent<GardenBehaviour>().saveMap();
            GameObject.FindWithTag("Inventory").GetComponent<InventoryBehaviour>().saveItems();
            GameObject.FindWithTag("Balance").GetComponent<Balance>().saveBalance();
        }
    }

    /* send request to Authentication server
       if successful then recieve JWT which is
       used for all further requests */
    public void login() {
        errorpanel.SetActive(false);

        string username = usernameInput.text;
        string password = passwordInput.text;

        WWWForm form = new WWWForm();
        form.AddField("username", username);
        form.AddField("password", password);
        form.AddField("issuer", "game.issuer");

        StartCoroutine(postRequest(LOGIN_URL, form, (UnityWebRequest request) => {
            try {
                auth = JsonUtility.FromJson<Authentication>(request.downloadHandler.text);
            } catch {
                auth = null;
            }

            if (auth != null && auth.auth && auth.token != null) {

                addLocalUser(username);
                // start users game
                setCurrentUser(username);

                print("Login success");
                if (rememberme.isOn) {
                    PlayerPrefs.SetString("uname", username);
                    PlayerPrefs.SetString("RememberMe", Encrypt.EncryptString(auth.token, secret_key));
                    PlayerPrefs.SetString("LoginTime", DateTime.Now.ToString());
                    PlayerPrefs.Save();
                } else {
                    PlayerPrefs.DeleteKey("uname");
                    PlayerPrefs.DeleteKey("RememberMe");
                    PlayerPrefs.Save();
                }
            } else {
                print(request.error);

                if (request.isHttpError) {
                    errorpanel.GetComponent<Text>().text = "Invalid username or password";
                    errorpanel.SetActive(true);
                } else if (request.isNetworkError) {
                    errorpanel.GetComponent<Text>().text = "Network Error";
                    errorpanel.SetActive(true);
                }

                usernameInput.text = "";
                passwordInput.text = "";
            }
        }));

    }

    public void logout() {
        saveAll();
        loaded = 0;
        auth = null;
        currentUser = null;
        if (PlayerPrefs.HasKey("RememberMe")) {
            PlayerPrefs.DeleteKey("RememberMe");
            PlayerPrefs.DeleteKey("uname");
        }
        SceneManager.LoadScene("Login", LoadSceneMode.Single);
    }


    /* sends a request containing the sparrow and birdbox state
       and saves locally */
    public void saveSparrows(SparrowDataCollection data) {
        string json = JsonUtility.ToJson(data);
        print("Saving Birdbox and Sparrows");

        // save locally
        PlayerPrefs.SetString(currentUser + "/Sparrows", Encrypt.EncryptString(json, secret_key));

        if (playingLocally) return;

        WWWForm form = new WWWForm();
        form.AddField("data", json);

        StartCoroutine(postRequestWithToken(SPARROW_URL, form, (UnityWebRequest request) => {
            print(request.error);
            print(request.downloadHandler.text);
        }));
    }

    /* requets sparrow and birdbox state from server
       if request fails and a local save is found then
       it is loaded */
    public void loadSparrows(BirdboxBehaviour birdbox) {
        SparrowDataCollection localSparrows = null;

        if (PlayerPrefs.HasKey(currentUser + "/Sparrows")) {
            localSparrows = JsonUtility.FromJson<SparrowDataCollection>(Encrypt.DecryptString(PlayerPrefs.GetString(currentUser + "/Sparrows"), secret_key));
            if (playingLocally) {
                birdbox.loadSparrows(localSparrows);
                finishedLoad();
                return;
            }
        }

        StartCoroutine(getRequestWithToken(SPARROW_URL, (UnityWebRequest request) => {
            SparrowDataCollection sparrowDatas;
            try {
                ResponseData data = JsonUtility.FromJson<ResponseData>(request.downloadHandler.text);
                sparrowDatas = JsonUtility.FromJson<SparrowDataCollection>(data.data);
            } catch {
                sparrowDatas = null;
            }
            // SparrowDataCollection sparrowDatas = JsonUtility.FromJson<SparrowDataCollection>(json);
            if (sparrowDatas != null) {
                print(JsonUtility.FromJson<ResponseData>(request.downloadHandler.text).data);

                // if both local garden and server garden found then choose the latest save
                if (localSparrows != null && localSparrows.savedTime > sparrowDatas.savedTime) {
                    birdbox.loadSparrows(localSparrows);
                } else {
                    birdbox.loadSparrows(sparrowDatas);
                }

            } else {
                print(request.error);
                print(request.downloadHandler.text);
                print("Loading default sparrows");
                // default sparrows
                birdbox.loadSparrows(localSparrows);
            }
            finishedLoad();
        }));
    }

    /* sends a request containing the garden state
       and saves locally */
    public void saveGarden(GardenData data) {
        string json = JsonUtility.ToJson(data);
        print("Saving Garden");

        // save locally
        PlayerPrefs.SetString(currentUser + "/Garden", Encrypt.EncryptString(json, secret_key));
        if (playingLocally) return;

        WWWForm form = new WWWForm();
        form.AddField("data", json);

        StartCoroutine(postRequestWithToken(GARDEN_URL, form, (UnityWebRequest request) => {
            print(request.error);
            print(request.downloadHandler.text);
        }));
    }

    /* requets garden state from server
       if request fails and a local save is found then
       it is loaded */
    public void loadGarden(GardenBehaviour garden) {
        GardenData localGarden = null;

        if (PlayerPrefs.HasKey(currentUser + "/Garden")) {
            string localJson = Encrypt.DecryptString(PlayerPrefs.GetString(currentUser + "/Garden"), secret_key);
            localGarden = JsonUtility.FromJson<GardenData>(localJson);
            if (playingLocally) {
                garden.loadMap(localGarden);
                finishedLoad();
                return;
            }
        }

        StartCoroutine(getRequestWithToken(GARDEN_URL, (UnityWebRequest request) => {
            GardenData gardenData;
            try {
                ResponseData data = JsonUtility.FromJson<ResponseData>(request.downloadHandler.text);
                print(data.data);
                gardenData = JsonUtility.FromJson<GardenData>(data.data);
            } catch {
                gardenData = null;
            }

            if (gardenData != null) {

                // if both local garden and server garden found then choose the latest save
                if (localGarden != null && localGarden.savedTime > gardenData.savedTime) {
                    garden.loadMap(localGarden);
                } else {
                    garden.loadMap(gardenData);
                }

            } else {
                print(request.error);
                print(request.downloadHandler.text);

                // default map for now
                garden.loadMap(localGarden);

            }
            finishedLoad();
        }));
    }

    /* sends a request containing the inventory state
       and saves locally */
    public void saveInventory(InventoryData data) {
        string json = JsonUtility.ToJson(data);
        print("Saving Inventory");

        // save locally
        PlayerPrefs.SetString(currentUser + "/Inventory", Encrypt.EncryptString(json, secret_key));
        if (playingLocally) return;

        WWWForm form = new WWWForm();
        form.AddField("data", json);

        StartCoroutine(postRequestWithToken(INVENTORY_URL, form, (UnityWebRequest request) => {
            print(request.error);
            print(request.downloadHandler.text);
        }));
    }

    /* requets inventory state from server
       if request fails and a local save is found then
       it is loaded */
    public void loadInventory(InventoryBehaviour inventory) {
        InventoryData localInventory = null;

        if (PlayerPrefs.HasKey(currentUser + "/Inventory")) {
            string localJson = Encrypt.DecryptString(PlayerPrefs.GetString(currentUser + "/Inventory"), secret_key);
            localInventory = JsonUtility.FromJson<InventoryData>(localJson);
            if (playingLocally) {
                inventory.loadItems(localInventory);
                finishedLoad();
                return;
            }
        }

        StartCoroutine(getRequestWithToken(INVENTORY_URL, (UnityWebRequest request) => {
            InventoryData data;
            try {
                ResponseData json = JsonUtility.FromJson<ResponseData>(request.downloadHandler.text);
                data = JsonUtility.FromJson<InventoryData>(json.data);
            } catch {
                data = null;
            }

            if (data != null) {
                print(request.downloadHandler.text);

                // if both local garden and server garden found then choose the latest save
                if (localInventory != null && localInventory.savedTime > data.savedTime) {
                    inventory.loadItems(localInventory);
                } else {
                    inventory.loadItems(data);
                }

            } else {
                print(request.error);
                inventory.loadItems(localInventory);
            }
            finishedLoad();
        }));
    }

    /* sends a request containing the coin delta since last response
       from server and saves locally */
    public void saveBalance(BalanceData data, Balance bal) {
        print("delta: " + data.delta);
        // perform one only request at a time to avoid race conditions
        if (savingBalance) {
            return;
        }
        savingBalance = true;
        print("Saving Balance");

        WWWForm form = new WWWForm();
        form.AddField("data", data.delta);
        PlayerPrefs.SetString(currentUser + "/Balance", Encrypt.EncryptString(JsonUtility.ToJson(data), secret_key));

        StartCoroutine(postRequestWithToken(BALANCE_URL, form, (UnityWebRequest request) => {
            int coins;
            try {
                ResponseBalanceData json = JsonUtility.FromJson<ResponseBalanceData>(request.downloadHandler.text);
                coins = json.balance;
            } catch {
                coins = -1;
            }

            if (coins >= 0) {
                print(request.downloadHandler.text);
                bal.setBalance(coins, 0);

                BalanceData updated = new BalanceData(coins, 0);
                PlayerPrefs.SetString(currentUser + "/Balance", Encrypt.EncryptString(JsonUtility.ToJson(updated), secret_key));
            } else {
                print(request.error);
                print(request.downloadHandler.text);

            }
            savingBalance = false;
            finishedLoad();

        }));


    }

    /* requets coins balance from server
       if request fails and a local save is found then
       it is loaded. If local delta is found it is sent
       to server */
    public void loadBalance(Balance bal) {

        if (PlayerPrefs.HasKey(currentUser + "/Balance")) {
            BalanceData localBalance = null;
            string localJson = Encrypt.DecryptString(PlayerPrefs.GetString(currentUser + "/Balance"),secret_key);
            localBalance = JsonUtility.FromJson<BalanceData>(localJson);
            if (playingLocally) {
                bal.setBalance(localBalance.coins, localBalance.delta);
                finishedLoad();
                return;
            }
            // send local balance to server
            if (localBalance.delta != 0) {
                saveBalance(localBalance, bal);
                return;
            }
        }

        StartCoroutine(getRequestWithToken(BALANCE_URL, (UnityWebRequest request) => {
            int coins;
            try {
                ResponseBalanceData json = JsonUtility.FromJson<ResponseBalanceData>(request.downloadHandler.text);
                coins = json.balance;
                print(json.balance);
            } catch {
                coins = -1;
            }


            if (coins >= 0) {
                print(request.downloadHandler.text);
                bal.setBalance(coins, 0);
                finishedLoad();
                BalanceData updated = new BalanceData(coins, 0);
                PlayerPrefs.SetString(currentUser + "/Balance", Encrypt.EncryptString(JsonUtility.ToJson(updated), secret_key));
            } else {
                print(request.error);
                print(request.downloadHandler.text);

                // default balance
                bal.setBalance(0, 1000);
                bal.saveBalance();
            }
        }));

    }

    /* sends a POST request to uri with form data attached.
       After response is recieved callBack is called and
       passed the UnityWebRequest object. */
    IEnumerator postRequest(string uri, WWWForm form, System.Action<UnityWebRequest> callBack) {
        if (form == null) form = new WWWForm();

        UnityWebRequest request = UnityWebRequest.Post(uri, form);
        yield return request.SendWebRequest();
        callBack(request);
    }

    /* sends a POST request to uri with form data body and JWT in header.
       After response is recieved callBack is called and passed the
       UnityWebRequest object. Can only be used if logged in */
    IEnumerator postRequestWithToken(string uri, WWWForm form, System.Action<UnityWebRequest> callBack) {
        if (auth == null) {
            yield break;
        }

        if (form == null) form = new WWWForm();

        UnityWebRequest request = UnityWebRequest.Post(uri, form);
        request.SetRequestHeader("Authorization", "Bearer " +  auth.token);

        yield return request.SendWebRequest();
        callBack(request);
    }

    /* sends a GET request to uri with JWT in header.
       After response is recieved callBack is called and passed the
       UnityWebRequest object. Can only be used if logged in */
    IEnumerator getRequestWithToken(string uri, System.Action<UnityWebRequest> callBack) {
        if (auth == null) {
            yield break;
        }

        UnityWebRequest request = UnityWebRequest.Get(uri);
        request.SetRequestHeader("Authorization", "Bearer " +  auth.token);

        yield return request.SendWebRequest();
        callBack(request);
    }

    /* Saves username locally so that local save data can be accessed */
    private void addLocalUser(string username) {
        UsersData users;
        if (PlayerPrefs.HasKey("Users")) {
            UsersData oldUsers = JsonUtility.FromJson<UsersData>(PlayerPrefs.GetString("Users"));
            // check if already has username
            if (Array.IndexOf(oldUsers.usernames, username) > -1) return;

            users = new UsersData(oldUsers.usernames, username);
        } else {
            users = new UsersData(username);
        }
        string json = JsonUtility.ToJson(users);
        print(json);
        PlayerPrefs.SetString("Users", json);
        // blanks for all saves
        PlayerPrefs.SetString(username + "/Sparrows", "");
        PlayerPrefs.SetString(username + "/Garden", "");
        PlayerPrefs.SetString(username + "/Inventory", "");
    }

    /* returns array of all usernames saved locally */
    public string[] getLocalUsers() {
        if (PlayerPrefs.HasKey("Users")) {
            UsersData users = JsonUtility.FromJson<UsersData>(PlayerPrefs.GetString("Users"));
            return users.usernames;
        }
        return null;
    }
}

/* Authentication object recieved from server */
[Serializable]
public class Authentication {
    public bool auth;
    public string token;
}

[Serializable]
public class ResponseData {
    public string data;
}

[Serializable]
public class ResponseBalanceData {
    public int balance;
}
