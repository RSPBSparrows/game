using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/* Objects that can be serialized to JSON for database and local storage */

[Serializable]
public class BalanceData {
    public int coins;
    public int delta;

    public BalanceData(Balance bal) {
        coins = bal.getCoins();
        delta = bal.getDelta();
    }

    public BalanceData(int c, int d) {
        coins = c;
        delta = d;
    }
}
