using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/* Objects that can be serialized to JSON for database and local storage */

[Serializable]
public class SparrowData {
    public string sparrowName;
    public double hunger;
    public double health;
    public double happiness;

    public SparrowData(SparrowBehaviour sparrow) {
        this.sparrowName = sparrow.getName();
        this.hunger = sparrow.getHunger();
        this.health = sparrow.getHealth();
        this.happiness = sparrow.getHappiness();
    }
}

[Serializable]
public class SparrowDataCollection {
    public DateTime savedTime = DateTime.Now;

    public int birdboxLevel;
    public SparrowData[] sparrowDatas;

    public SparrowDataCollection(List<SparrowData> sparrowDataList, BirdboxBehaviour birdbox) {
        sparrowDatas = sparrowDataList.ToArray();
        birdboxLevel = birdbox.getLevel();
    }
}
