using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/* Objects that can be serialized to JSON for database and local storage */

[Serializable]
public class GardenData {
    public DateTime savedTime = DateTime.Now;

    public int columns;
    public int rows;
    public PlaceableItemData[] ground;
    public PlaceableItemData[] items;

    public GardenData(GardenBehaviour garden, List<PlaceableItemData> ground, List<PlaceableItemData> items) {
        rows = garden.getRows();
        columns = garden.getColumns();
        this.ground = ground.ToArray();
        this.items = items.ToArray();
    }
}

[Serializable]
public class PlaceableItemData {
    public Vector2 tilePos;
    public string prefab;
    // json of individual item state data
    public string data;

    public PlaceableItemData(PlaceableItem item) : this(item, "") { }

    public PlaceableItemData(PlaceableItem item, string data) {
        this.tilePos = item.tilePos;
        this.prefab = item.prefabName;
        this.data = data;
    }
}
