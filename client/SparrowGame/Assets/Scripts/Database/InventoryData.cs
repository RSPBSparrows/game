using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/* Objects that can be serialized to JSON for database and local storage */

[Serializable]
public class InventoryData {
    public DateTime savedTime = DateTime.Now;

    public InventoryItemData[] items;

    public InventoryData(List<InventoryItemData> itemDatas) {
        items = itemDatas.ToArray();
    }
}

[Serializable]
public class InventoryItemData {
    public int quantity;
    public string prefab;

    public InventoryItemData(GameObject obj, int quantity) {
        prefab = obj.name;
        this.quantity = quantity;
    }
}
