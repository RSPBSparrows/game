using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Balance: MonoBehaviour {

	public GameObject balanceChangePanel;

	private Text balance;
	private int coins;
	private int delta;

	private int localDelta;

	int frame = 1;

	DataControllerBehaviour dataController;

	void Awake() {
		balance = this.gameObject.GetComponent < Text > ();
		dataController = GameObject.FindWithTag("DataController").GetComponent < DataControllerBehaviour > ();
		// try and load from profile
		load();
		updateBalance();
	}

	void Update() {
		updateBalance();
		if (frame % 1000 == 0) {
			saveBalance();
		}
		frame++;
	}

	// show how many coins you have
	private void updateBalance() {
		balance.text = getBalance().ToString();
	}

	// Increase or decrease balance
	private void changeBalance(int c) {
		if (c == 0) return;
		delta += c;
		localDelta += c;
		updateBalance();

		GameObject panel = Instantiate(balanceChangePanel, transform) as GameObject;
		panel.GetComponent<BalanceChangePanel>().setChange(c);
	}

	public void increase(int c) {
		changeBalance(c);
	}

	public bool decrease(int c) {
		if (getBalance() >= c) {
			changeBalance(-c);
			return true;
		}
		return false;
	}

	// Getters and setters
	public int getBalance() {
		return coins + delta;
	}

	public int getDelta() {
		return delta;
	}

	// use getBalance for actual number of coins
	public int getCoins() {
		return coins;
	}

	public bool canAfford(int c) {
		return getBalance() >= c;
	}

	/* Create json object of balance and send to server
	   store localDelta to track changes between sending
	   and recieving response */
	public void saveBalance() {
		localDelta = 0;
		dataController.saveBalance(new BalanceData(this), this);
	}

	public void setBalance(int c, int d) {
		coins = c;
		delta = d + localDelta;
		localDelta = 0;
		updateBalance();
	}

	public void load() {
		dataController.loadBalance(this);
	}
}
