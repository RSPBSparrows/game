using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class NotificationsBehaviour : MonoBehaviour {

    public NotificationListPanel listPanel;

    private List<Notification> notifications;

    void Start() {
        notifications = new List<Notification>();
    }

    /* Add Notification without specifying timeout */
    public Notification addNotification(string message, int priority) {
        return addNotification(message, priority, -1);
    }

    /* Add Notification returning the Notification object which can then be
       passed to deleteNotification. If a duplicate notification exists don't
       add, return the duplicate. */
    public Notification addNotification(string message, int priority, int timeout) {
        Notification duplicate = findNotification(message, priority);
        if (duplicate != null) {
            return duplicate;
        }

        Notification notif = new Notification(message, priority, timeout);
        notifications.Add(notif);
        if (notif.getTimeout() > 0) {
            StartCoroutine(timeoutNotification(notif));
        }
        updatePanel();
        return notif;
    }

    /* Remove the given notification if it exists */
    public Notification deleteNotification(Notification notif) {
        notifications.Remove(notif);
        notif = null;
        updatePanel();
        return null;
    }

    /* Return a List of Notifications in increasing priority order */
    public List<Notification> getNotifications() {
        return notifications.OrderBy(n=>n.getPriority()).ToList();
    }

    /* Redraw notifications */
    public void updatePanel() {
        listPanel.update();
    }

    /* Search notifications for one with given message and priority */
    private Notification findNotification(string message, int priority) {
        Notification notif;
        for (int i = 0; i < notifications.Count; i++) {
            notif = notifications[i];
            if (notif.getMessage().Equals(message) && notif.getPriority() == priority) {
                return notif;
            }
        }
        return null;
    }

    /* Delete a notification after its timeout */
    IEnumerator timeoutNotification(Notification notif) {
        yield return new WaitForSeconds(notif.getTimeout());
        deleteNotification(notif);
        yield return null;
    }

}
