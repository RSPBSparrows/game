public class Notification {

    private string message;
    // priority to determine icon and place on list
    // lower is higher
    private int priority;
    private int timeout;

    public Notification(string message, int priority) : this(message, priority, -1) {}

    public Notification(string message, int priority, int timeout) {
        this.message = message;
        this.priority = priority;
        this.timeout = timeout;
    }

    public string getMessage() {
        return message;
    }

    public int getPriority() {
        return priority;
    }

    public int getTimeout() {
        return timeout;
    }

}
