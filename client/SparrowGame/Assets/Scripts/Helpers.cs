using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using UnityEngine;

// Used to get certain gameObjects
public static class Helpers {

	// returns the index of the largest element of arr
	public static int IndexOfMax(double[] arr) {
		if (arr.Length < 1) return -1;

		int maxIndex = 0;
		double max = arr[0];

		for (int i = 1; i < arr.Length; i++) {
			if (arr[i] > max) {
				max = arr[i];
				maxIndex = i;
			}
		}
		return maxIndex;
	}

	// returns the index of the largest element of arr
	public static int IndexOfMax(int[] arr) {
		if (arr.Length < 1) return -1;

		int maxIndex = 0;
		int max = arr[0];

		for (int i = 1; i < arr.Length; i++) {
			if (arr[i] > max) {
				max = arr[i];
				maxIndex = i;
			}
		}
		return maxIndex;
	}

	// returns true if d is greater than or equal to the
	// maximum element in arr
	public static bool IsMax(double d, double[] arr) {
		for (int i = 0; i < arr.Length; i++) {
			if (arr[i] > d) return false;
		}
		return true;
	}

	// return 1  if f >= 0
	// return -1 if f < 0
	public static float SignOf(float f) {
		if (f >= 0) return 1;
		return -1;
	}

	// returns true if t1 is within dist radius of t2
	public static bool IsNearby(Transform t1, Transform t2, float dist) {
		return IsNearby(t1.position.x, t1.position.y, t2.position.x, t2.position.y, dist);
	}

	// return true if (x1, y1) is within dist radius of (x2, y2)
	public static bool IsNearby(float x1, float y1, float x2, float y2, float dist) {
		return Math.Pow(dist, 2) >= Math.Pow(x2 - x1, 2) + Math.Pow(y2 - y1, 2);
	}

	// returns straight line distance from (x1, y1) to (x2, y2)
	public static double DistanceBetween(float x1, float y1, float x2, float y2) {
		return Math.Sqrt(Math.Pow(x2 - x1, 2) + Math.Pow(y2 - y1, 2));
	}

	// returns the Transform of a random direct child of a GameObject
	public static Transform ChooseRandomChild(GameObject holder) {
		if (holder.transform.childCount < 1) return null;
		return holder.transform.GetChild((new System.Random()).Next(0, holder.transform.childCount));
	}

	public static GameObject ChooseRandomWithTag(string tag, System.Random rnd) {
		GameObject[] objs = GameObject.FindGameObjectsWithTag(tag);
		if (objs.Length < 1) return null;
		return objs[rnd.Next(0, objs.Length)];
	}

	public static GameObject ChooseRandomGameObjectWithTags(string[] tags, System.Random rnd) {
		List<GameObject> allPossible = new List<GameObject>();

		foreach (string tag in tags) {
			GameObject[] objs = GameObject.FindGameObjectsWithTag(tag);
			if (objs.Length > 0) {
				List<GameObject> lst = objs.OfType<GameObject>().ToList();
				allPossible.AddRange(lst);
			}

		}

		if (allPossible.Count <= 0) {
			return null;
		}
		return allPossible[rnd.Next(0, allPossible.Count)];
	}


	// finds the closest direct child of holder to (x, y)
	public static Transform FindNearestChild(float x, float y, GameObject holder) {
		if (holder.transform.childCount < 1) return null;

		Transform currentChild = holder.transform.GetChild(0);
		Transform minChild = currentChild;
		double minDist = DistanceBetween(x, y, currentChild.transform.position.x, currentChild.transform.position.y);
		double dist;

		for (int i = 1; i < holder.transform.childCount; i++) {
			currentChild = holder.transform.GetChild(i);
			dist = DistanceBetween(x, y, currentChild.position.x, currentChild.position.y);
			if (dist < minDist) {
				minChild = currentChild;
				minDist = dist;
			}
		}
		return minChild;
	}

	// finds the closest GameObject in arr to (x, y)
	public static Transform FindNearestFromArray(float x, float y, GameObject[] arr) {
		if (arr.Length < 1) return null;

		Transform currentObj = arr[0].transform;
		Transform minObj = currentObj;
		double minDist = DistanceBetween(x, y, currentObj.position.x, currentObj.position.y);
		double dist;

		for (int i = 1; i < arr.Length; i++) {
			currentObj = arr[i].transform;
			dist = DistanceBetween(x, y, currentObj.position.x, currentObj.position.y);
			if (dist < minDist) {
				minObj = currentObj;
				minDist = dist;
			}
		}
		return minObj;
	}

	public static Vector3 FromToDirection(Vector3 p1, Vector3 p2) {
		float gap = (float) DistanceBetween(p1.x, p1.y, p2.x, p2.y);
		return new Vector3((p2.x - p1.x) / gap, (p2.y - p1.y) / gap, 0);
	}

	public static int GetSortingOrder(GameObject obj) {
		if (obj == null) return 0;
		return 25 - (int) obj.transform.position.y;
	}
}
