using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Manages click priority of gameobjects
public class PerspectiveSortingLayerBehaviour: MonoBehaviour {

	public int priority;
	private SpriteRenderer sprite;

	void Awake() {
		sprite = GetComponent < SpriteRenderer > ();
	}

	// Update is called once per frame
	void Update() {
		if (sprite) {
			// smaller y should have higher sorting order to appear in front
			sprite.sortingOrder = 1000 - 2 * (int) transform.position.y + priority;
		}
	}
}
