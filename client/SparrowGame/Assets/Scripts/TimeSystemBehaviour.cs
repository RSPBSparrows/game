using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Random = UnityEngine.Random;

// Time framing for the game
public class TimeSystemBehaviour: MonoBehaviour {

	public float dayInterval;
	public float nightInterval;
	public float chanceInterval;
	float dayTime;
	float chanceTime;

	private static System.Random rnd;
	private BirdboxBehaviour birdbox;
	double chance;

	public bool isDay;
	public GameObject NightPanel;
	public GameObject superhot;

	private KonamiCode code;
	static bool inkonami;
	// Use this for initialization
	void Start() {
		dayInterval = 300.0f; // set interval for 5 minutes
		nightInterval = 60f; // 1 min
		dayTime = dayInterval; // set actual time left until next update
		chanceInterval = 30.0f; // set interval for 30 secs for sparrow spawning chance
		chanceTime = chanceInterval; // set actual time left until next update
		chance = 0;
		//rnd = new System.Random();
		isDay = true;
		birdbox = GameObject.FindWithTag("Birdbox").GetComponent < BirdboxBehaviour > ();
		code = GetComponent < KonamiCode > ();
	}

	// Update is called once per frame
	void Update() {
		if (!inkonami && code.success) {
			inkonami = true;
			print("ohboi");
			StartCoroutine(superbirb());
		}

		// quit on back button
		if (Input.GetKeyDown(KeyCode.Escape)) {
			print("Save and quit");
			// save then quit
			Application.Quit();
		}
		dayTime -= Time.deltaTime; // subtract the time taken to render last frame
		chanceTime -= Time.deltaTime;

		// if time runs out, change day night cycle
		if(dayTime <= 0) {
			isDay = !isDay;
			StartCoroutine(FadeImage(isDay));

			if(isDay){
				dayTime = nightInterval;
			}
			else{
				dayTime = dayInterval; // reset the timer
			}
		}
		// Chance to spawn a sparrow every 30 secs
		if(chanceTime <= 0) {
			int noSparrows = birdbox.getNumSparrows();
			int maxSparrows = birdbox.getMaxSparrows();
			//chance increase
			if (noSparrows < maxSparrows && chance < 1)
			chance += 0.2; //20% chance

			//chance to spawn
			if (Random.value < chance) {
				birdbox.spawnSparrow();
				chance = 0;
			}
			chanceTime = chanceInterval; // reset the timer
		}
	}

	// If its day or night
	public bool getDay() {
		return isDay;
	}

	// Slowly becoming night or day
	IEnumerator FadeImage(bool fadeAway) {
		CanvasGroup img = NightPanel.GetComponent < CanvasGroup > ();
		// fade from opaque to transparent
		if (fadeAway) {
			// loop over 1 second backwards
			for (float i = 1; i >= 0; i -= (Time.deltaTime * 0.1f)) {
				// set color with i as alpha
				img.alpha = i;
				yield
				return null;
			}
		}
		// fade from transparent to opaque
		else {
			for (float i = 0; i <= 1; i += (Time.deltaTime * 0.1f)) {
				img.alpha = i;
				yield return null;
			}
		}
	}

	// Ignore
	// For konami code
	IEnumerator superbirb() {
		// super
		superhot.SetActive(true);
		superhot.transform.GetChild(0).gameObject.SetActive(true);
		yield return new WaitForSeconds(0.7f);
		// birb
		superhot.transform.GetChild(0).gameObject.SetActive(false);
		superhot.transform.GetChild(1).gameObject.SetActive(true);
		yield return new WaitForSeconds(0.7f);
		// super
		superhot.transform.GetChild(1).gameObject.SetActive(false);
		superhot.transform.GetChild(0).gameObject.SetActive(true);
		yield return new WaitForSeconds(0.7f);
		// birb
		superhot.transform.GetChild(0).gameObject.SetActive(false);
		superhot.transform.GetChild(1).gameObject.SetActive(true);
		yield return new WaitForSeconds(0.7f);

		superhot.SetActive(false);
		yield return null;
	}

	public bool iskonami() {
		return inkonami;
	}

}
