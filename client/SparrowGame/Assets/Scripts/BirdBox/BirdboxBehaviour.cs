using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random=UnityEngine.Random;

public class BirdboxBehaviour : MonoBehaviour {

    string[] names = new string[] {
        "Potato", "Dimitri", "Brownbirb", "Tiki", "Tweety", "Gabby", "Merlin", "Coco", "Lucky", "Max",
        "Peaches", "Kiki", "Buddy", "Bobo", "Rio", "Abu", "Bibi", "Chip", "Chirpy", "Elvis", "Freddie",
        "Jabba", "Kobie", "Piki", "Skip", "Peanut"
    };

    private DataControllerBehaviour dataController;

    public GameObject sparrowPrefab;
    public Sprite[] sprite_stages;
    private SpriteRenderer sprite;

    // current and maximum no. of sparrows
    private int num_sparrows;
    private int max_sparrows;

    // current, min and max upgrade level
    private int level;
    private int min_level = 0;
    private int max_level = 6;

    private System.Random rnd = new System.Random();

    void Awake () {
        sprite = GetComponent<SpriteRenderer>();
        dataController = GameObject.FindWithTag("DataController").GetComponent<DataControllerBehaviour>();
        dataController.loadSparrows(this);
    }

    /* Spawn sparrows using the SparrowData for their stats and
       Load the birdbox level */
    public void loadSparrows(SparrowDataCollection data) {
        // load sparrows and level from DB
        if (data != null) {
            setLevel(data.birdboxLevel, false);

            for (int i = 0; i < data.sparrowDatas.Length; i++) {
                SparrowData spData = data.sparrowDatas[i];
                spawnSparrow(spData.sparrowName, spData.hunger, spData.health, spData.happiness);
            }
            num_sparrows = data.sparrowDatas.Length;
        } else {
            // create default
            setLevel(min_level);
            addStartingSparrows(1);
        }
    }

    /* Save all sparrows into one Serializable object and pass to
       DataController to be saved locally and sent to server */
    public void saveSparrows() {
        GameObject[] sparrows = getAllSparrows();
        List<SparrowData> sparrowDatas = new List<SparrowData>();
        for (int i = 0; i < sparrows.Length; i++) {
            sparrowDatas.Add(new SparrowData(sparrows[i].GetComponent<SparrowBehaviour>()));
        }
        SparrowDataCollection datas = new SparrowDataCollection(sparrowDatas, this);
        dataController.saveSparrows(datas);
    }

    /* Spawn some sparrows */
    void addStartingSparrows(int num) {
        for (int i = 0; i < num; i++) {
            spawnSparrow();
        }
    }

    /* Spawn a sparrow with default stats */
    public void spawnSparrow() {
        spawnSparrow(names[Random.Range(0, names.Length)], rnd.Next(10, 20)/100.0f, 0.75f, 0.35f);
    }

    /* Spawn a sparrow with specified stats */
    public void spawnSparrow(string name, double hunger, double health, double happiness) {
        if (num_sparrows >= max_sparrows) return;
        GameObject newSparrow = Instantiate(sparrowPrefab) as GameObject;
        SparrowBehaviour behaviour = newSparrow.GetComponent<SparrowBehaviour>();
        behaviour.setSparrowStats(name, hunger, health, happiness);
    }

    /* Called once per frame */
    void Update() {
        int sparrow_count = getAllSparrows().Length;
        // when number of sparrows changes then save
        if (sparrow_count != num_sparrows) {
            saveSparrows();
        }
        num_sparrows = sparrow_count;
    }

    /* get max number of sparrows given the birdbox level */
    public int levelToMaxSparrows(int lvl) {
        if (lvl == 0) return 1;
        return lvl + 2;
    }

    public int getNumSparrows(){
        return num_sparrows;
    }

    public int getLevel() {
        return level;
    }

    public int getMaxSparrows() {
      return max_sparrows;
    }

    /* set birdbox level and save unless told not to */
    public void setLevel(int lvl, bool save=true) {
        if (lvl > max_level) {
            lvl = max_level;
        } else if (lvl < min_level) {
            lvl = min_level;
        }
        level = lvl;
        max_sparrows = levelToMaxSparrows(level);
        updateSprite();

        if (save) {
            saveSparrows();
        }
    }

    /* Change the sprite to match the birdbox level and resize collider to match */
    public void updateSprite() {
        BoxCollider2D collider = GetComponent<BoxCollider2D>();
        if (level == 0) {
            sprite.enabled = false;
            collider.enabled = false;
            return;
        }
        sprite.enabled = true;
        collider.enabled = true;
        sprite.sprite = sprite_stages[level-1];

        // update size of collider
        Vector2 S = sprite.sprite.bounds.size;
        collider.size = S;
    }

    /* Get an array of all sparrows in the game */
    public GameObject[] getAllSparrows() {
        return GameObject.FindGameObjectsWithTag("Sparrow");
    }

    public int getMaxLevel() {
        return max_level;
    }

    public int getMinLevel() {
        return min_level;
    }

}
