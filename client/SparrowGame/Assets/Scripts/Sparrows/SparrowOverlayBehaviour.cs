using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SparrowOverlayBehaviour : MonoBehaviour {

	SparrowBehaviour sparrow;
	GameObject nameObject;
	GameObject imageObject;
	private TimeSystemBehaviour time;
	public Sprite sad;
	public Sprite pepe;
	private AudioSource audio;

	private NotificationsBehaviour notifs;
	private Notification cautionSad;
	private const string SADMESSAGE = "Your sparrows are unhappy! Make sure they have enough food and try improving your garden!";

	void Awake() {
		audio = GetComponent<AudioSource>();
		notifs = GameObject.FindWithTag("Notifications").GetComponent<NotificationsBehaviour>();
		sparrow = this.gameObject.GetComponent<SparrowBehaviour>();
		nameObject = this.transform.GetChild(0).gameObject;
		imageObject = this.transform.GetChild(1).gameObject;
		time = GameObject.FindWithTag("TimeSystem").GetComponent<TimeSystemBehaviour>();
	}

	void Update() {
		// don't show names and faces if hiding
		if (sparrow.isSafe()) {
			imageObject.GetComponent<SpriteRenderer>().enabled = false;
			nameObject.SetActive(false);
			return;
		}
		// Change sprite based on konami
		imageObject.GetComponent<SpriteRenderer>().enabled = true;
		if(time.iskonami()){
			imageObject.GetComponent<SpriteRenderer>().sprite = pepe;
		}
		else{
			imageObject.GetComponent<SpriteRenderer>().sprite = sad;
		}

		//Find the sparrow and set the overlay just above it
		Vector3 pos = this.gameObject.transform.position;
		nameObject.transform.position = new Vector3(pos.x, pos.y + 0.2f, 2);
		nameObject.GetComponent<TextMesh>().text = sparrow.getName();
		imageObject.transform.position = new Vector3(pos.x, pos.y + 0.5f, 0);
		//When to show that the sparrow is sad
		if (sparrow.getHealth() < 0.3 || sparrow.getHappiness() < 0.3  || sparrow.getHunger() > 0.6 ){
			nameObject.transform.position = new Vector3(pos.x, pos.y + 0.8f, 0);
			imageObject.SetActive(true);
			if (cautionSad == null)
			cautionSad = notifs.addNotification(SADMESSAGE, 1);
		} else {
			imageObject.SetActive(false);
			cautionSad = notifs.deleteNotification(cautionSad);
		}
	}

	//Show overlay when clicked on sparrow
	void OnMouseUp() {
		audio.Play();
		nameObject.SetActive(!nameObject.activeSelf);
	}
}
