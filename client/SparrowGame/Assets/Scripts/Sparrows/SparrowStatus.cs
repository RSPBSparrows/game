﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SparrowStatus : MonoBehaviour {

	private SparrowBehaviour sparrow;
	public string sparrowName;

	void Start () {
		this.sparrow = GetComponent<SparrowBehaviour>();
	}

	void Update(){
		sparrowName = getName();
	}
	//Getters and setters for sparrow stats
	public double getHunger() {
		return sparrow.getHunger();
	}

	public double getHealth() {
		return sparrow.getHealth();
	}

	public double getHappiness() {
		return sparrow.getHappiness();
	}

	public string getName() {
		return sparrow.getName();
	}

	public void setName(string n) {
		sparrow.setName(n);
	}
}
