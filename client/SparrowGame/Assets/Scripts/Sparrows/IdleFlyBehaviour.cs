using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Random=UnityEngine.Random;

/* Taken from SparrowBehaviour for use on Login Screen */
public class IdleFlyBehaviour : MonoBehaviour {
    // used for changing animation
	private bool flying;
	private Direction facing;
	enum Direction {NORTH, EAST, SOUTH, WEST};
	private Animator animator;
    private float maxSpeed = 0.05f;
	private float minSpeed = 0.002f;
    private float turnSpeed = 0.001f;
    public GameObject smokebomb;
    private int clickcount = 0;

	// bounds for idle flying
	public Vector2 maxPos = new Vector2(17, 13);
	public Vector2 minPos = new Vector2(5, 5);
	// current position
	private Vector2 pos;

	// current speed
	private Vector2 speed;

	// current direction
	private Vector2 direction;
	private static System.Random rnd;

    void Start () {
		animator = GetComponent<Animator>();
		rnd = new System.Random();

		direction = new Vector2(chance(50) ? 1 : -1, chance(50) ? 1 : -1);
		speed = new Vector2(0.02f * direction.x, 0.02f * direction.y);
	}

    private bool chance(double percent) {
		return Random.value * 100 < percent;
	}

    void Update(){
        pos = transform.position;
        idleFly();
        changeSpriteAnimation();
    }

    void idleFly() {

		// if outside area then turn around
		if (pos.x > maxPos.x || pos.x < minPos.x) {
			direction.x = pos.x > maxPos.x ? -1 : 1;
		}

		// changing direction already
		if (Mathf.Sign(speed.x) != direction.x) {
			speed.x += turnSpeed * direction.x;
		} else {
			// if going less than half max speed then speed up
			// also chance to speed up after
			if (Math.Abs(speed.x) < maxSpeed/2 || chance(5)) {
				speed.x += turnSpeed * Mathf.Sign(speed.x);
			}
			// % chance to change direction at any point
			if (chance(0.3)) {
				direction.x *= -1;
			}
		}

		// if outside area then turn around
		if (pos.y > maxPos.y || pos.y < minPos.y) {
			direction.y = (pos.y > maxPos.y ? -1 : 1);
		}

		// changing direction already
		if (Mathf.Sign(speed.y) != direction.y) {
			this.speed.y += turnSpeed * direction.y;
		} else {
			// if going less than half max speed then speed up
			// also chance to speed up after
			if (Math.Abs(speed.y) < maxSpeed/2 || chance(5)) {
				this.speed.y += turnSpeed * Mathf.Sign(speed.y);
			}
			// % chance to change direction at any point
			if (chance(0.3)) {
				direction.y *= -1;
			}
		}

		correctSparrowSpeed();
		transform.position += new Vector3(speed.x, speed.y, 0);
	}

    void correctSparrowSpeed() {
		// don't exceed the speed limit
		if (Math.Abs(speed.x) > maxSpeed) {
			speed.x = maxSpeed * Mathf.Sign(speed.x);
		}
		if (Math.Abs(speed.y) > maxSpeed) {
			speed.y = maxSpeed * Mathf.Sign(speed.y);
		}
	}

    void changeSpriteAnimation() {
        if (speed.x > 0 && speed.x > Math.Abs(speed.y)) {
            facing = Direction.EAST;
            animator.Play("maleSparrowFlyingRight");
        } else if (speed.x < 0 && -speed.x > Math.Abs(speed.y)) {
            facing = Direction.WEST;
            animator.Play("maleSparrowFlyingLeft");
        } else if (speed.y > 0 && speed.y >= Math.Abs(speed.x)) {
            facing = Direction.NORTH;
            animator.Play("maleSparrowFlyingUp");
        } else if (speed.y < 0 && -speed.y >= Math.Abs(speed.x)) {
            facing = Direction.SOUTH;
            animator.Play("maleSparrowFlyingDown");
        }
    }

    void OnMouseDown(){
        maxSpeed += 0.01f;
        turnSpeed += 0.005f;

		direction = new Vector2(chance(50) ? 1 : -1, chance(50) ? 1 : -1);
        speed = direction.normalized*maxSpeed;
        clickcount++;

        if(clickcount >= 20){
            Instantiate(smokebomb, this.transform.position, Quaternion.identity);
		    Destroy(this.gameObject);
        }
    }
}
