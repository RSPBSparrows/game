﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Random=UnityEngine.Random;

public class SparrowBehaviour : MonoBehaviour {

	// SPARROW STATS (persistent through save)
	private string sparrowName;

	private double hunger;
	private double health;
	private double happiness;
	// end sparrow stats

	public GameObject smokebomb;

	private bool veryhappy;

	private BirdboxBehaviour birdbox;
	private TimeSystemBehaviour time;
	private Animator animator;
	private NotificationsBehaviour notifs;
	private Notification sparrowNotification;

	// used for changing animation
	private bool flying;
	private Direction facing;
	enum Direction {NORTH, EAST, SOUTH, WEST};

	// things for sparrow to interact with
	private GameObject foodTarget;
	private GameObject sitTarget;
	private GameObject safetyTarget;
	private GameObject nestTarget;
	private bool atFoodTarget = false;
	private bool atSitTarget = false;
	private bool atSafetyTarget = false;
	private bool atNestTarget = false;

	// current position
	private Vector2 pos;
	// current speed
	private Vector2 speed;
	// current direction
	private Vector2 direction;

	// maximum idle flying speed
	private const float maxIdleSpeed = 0.05f;
	private const float maxRunSpeed = maxIdleSpeed * 2;
	// current max speed
	private float maxSpeed = maxIdleSpeed;
	private float minSpeed = 0.002f;

	// bounds for idle flying
	private Vector2 maxPos = new Vector2(17, 13);
	private Vector2 minPos = new Vector2(5, 5);

	// where sparrow wants to go
	private Vector2 destination;

	// current state of sparrow
	public STATE state;
	// next state if current is flying to destination
	public STATE nextState;
	public enum STATE {IDLEFLY, FLYTODEST, EAT, SIT, NEST, HIDE, NULL};

	// scores to control which state sparrow is in
	public double[] stateScore = new double[6];

	// while lockFrames > 0 then state will not change unless
	// triggerStateChange() is called
	private int frame = 1;
	private int lockFrames;

	// safe from predators
	private bool safe;

	private static System.Random rnd;

	public double getHealth() {
		return health;
	}

	public double getHunger() {
		return hunger;
	}

	public double getHappiness() {
		return happiness;
	}

	public string getName() {
		return sparrowName;
	}

	public void setName(string n) {
		sparrowName = n;
		name = "Sparrow: " + n;
	}

	public bool isSafe() {
		return safe;
	}

	/* Allow the sparrow to change state:
	   guarantees that next frame makeDecision
	   runs */
	public void triggerStateChange() {
		lockFrames = 0;
	}

	/* Initialisation */
	void Start () {
		animator = GetComponent<Animator>();
		rnd = new System.Random();

		initialiseDefaultState();

		lockFrames = 0;
	}

	/* Initialisation */
	void Awake () {
		notifs = GameObject.FindWithTag("Notifications").GetComponent<NotificationsBehaviour>();
		time = GameObject.FindWithTag("TimeSystem").GetComponent<TimeSystemBehaviour>();
		birdbox = GameObject.FindWithTag("Birdbox").GetComponent<BirdboxBehaviour>();
		notifs = GameObject.FindWithTag("Notifications").GetComponent<NotificationsBehaviour>();
		updateBounds();
	}

	/* Called once per frame */
	void Update () {
		pos = transform.position;

		// get information from the map
		lookAround();
		// choose state
		makeDecision();
		// perform state action
		actOnState();
		// update sprite to match
		changeSpriteAnimation();
		// decay sparrows stats
		updateStats();
		// check sparrow still alive
		if(getHealth() <= 0) {
			kill(getName() + " has starved to death!");
		}

		if (lockFrames > 0) {
			lockFrames--;
		}
		frame++;
	}

	/* Called when a collider2D with Rigidbody2D overlaps sparrow
	   for longer than 1 frame. This is used to check when a destination
	   is reached */
	void OnTriggerStay2D(Collider2D coll) {
		// only collide with non-triggers
		if (coll.isTrigger) return;

		// still looking for something
		atFoodTarget = false;
		atSafetyTarget = false;
		atSitTarget = false;
		atNestTarget = false;

		// check if still at target
		if ((state == STATE.EAT || nextState == STATE.EAT)&& coll.gameObject == foodTarget) {
			atFoodTarget = true;
		} else if ((state == STATE.HIDE || nextState == STATE.HIDE) && coll.gameObject == safetyTarget) {
			atSafetyTarget = true;
		} else if ((state == STATE.SIT || nextState == STATE.SIT) && coll.gameObject == sitTarget && Vector2.Distance(pos, sitTarget.transform.position) < 0.6f) {
			atSitTarget = true;
		} else if ((state == STATE.NEST || nextState == STATE.NEST) && coll.gameObject == nestTarget) {
			atNestTarget = true;
		}
	}


	/* Check the garden for events */
	void lookAround() {
		if (atFrame(500)) {
			updateBounds();
		}
		// if there is a predator then react to it unless already hidden
		if (GameObject.FindWithTag("Predator") && state != STATE.HIDE) {
			GameObject[] predators = GameObject.FindGameObjectsWithTag("Predator");
			foreach (GameObject predator in predators) {
				if (!predator.GetComponent<CatBehaviour>().isHidden()) {
					lockFrames = 0;
					break;
				}
			}
		}
	}

	/* Update the idleflying limits based on the size of garden */
	void updateBounds() {
		GardenBehaviour garden = GameObject.FindWithTag("Garden").GetComponent<GardenBehaviour>();
		maxPos.x = (int)garden.getNE().x + 3;
		maxPos.y = (int)garden.getNE().y + 4;
		minPos.x = (int)garden.getSW().x - 3;
		minPos.y = (int)garden.getSW().y - 2;
	}

	/* Default starting state */
	void initialiseDefaultState() {
		state = STATE.IDLEFLY;
		direction = new Vector2(chance(50) ? 1 : -1, chance(50) ? 1 : -1);
		speed = new Vector2(0.02f * direction.x, 0.02f * direction.y);
	}

	/* Used to initialise a sparrow with specific stats when loaded */
	public void setSparrowStats(string name, double hunger, double health, double happiness) {
		sparrowName = name;
		this.hunger = hunger;
		this.health = health;
		this.happiness = happiness;
		this.name = name;
	}

	/* Choose state for next action based on stats and garden
	   A score in range [0,1] is put in to stateScore array
	   and the state with the highest score at the end is
	   chosen as the current state */
	void makeDecision() {
		// if state is locked (also don't lock state when flying to a destination)
		if (lockFrames > 0 && state != STATE.FLYTODEST) {
			atSafetyTarget = false;
			atNestTarget = false;
			atFoodTarget = false;
			atSitTarget = false;
			return;
	 	}

		// starting scores for each state
		double idleFlyScore = 0.39;
		double flyToDestScore = 0;
		double eatScore = 0;
		double sitScore = 0.39;
		double nestScore = 0.39;
		double hideScore = 0;
		// next destination default to current destination
		Vector2 nextDestination = new Vector2(destination.x, destination.y);

		// Choose preferred state

		// check for close by predators
		if (GameObject.FindWithTag("Predator")) {
			GameObject[] predators = GameObject.FindGameObjectsWithTag("Predator");
			foreach (GameObject predator in predators) {
				if (!predator.GetComponent<CatBehaviour>().isHidden()) {
					hideScore = 0.9;
					break;
				}
			}
		}

		// if eating keep eating until not hungry
		if (state == STATE.EAT && foodTarget != null && foodTarget.GetComponent<Food>().hasFood()) {
			if (hunger > 0.001) {
				eatScore = 0.5;
			} else {
				eatScore = 0.0;
			}
		} else {
			// score scales with hunger
			eatScore = hunger;
		}

		updateScores(idleFlyScore, flyToDestScore, eatScore, sitScore, nestScore, hideScore);

		// Chosen preferred state now check conditions for entering state
		// and fly to target if necessary

		// Want to hide from a predator
		if (Helpers.IsMax(hideScore, stateScore)) {
			// find somewhere to hide
			GameObject[] possible = findSafetyTargets();
			safetyTarget = possible.Length > 0 ? Helpers.FindNearestFromArray(pos.x, pos.y, possible).gameObject : null;

			if (safetyTarget != null) {
				// check if already there
				if (atSafetyTarget) {
					flyToDestScore = 0.0;
				} else {
					// fly to safety
					flyToDestScore = hideScore;
					nextState = STATE.HIDE;
					hideScore = 0.0;
					nextDestination.x = safetyTarget.transform.position.x;
					nextDestination.y = safetyTarget.transform.position.y+0.25f;
				}
			} else {
				// has nowhere to hide so can't :(
				hideScore = 0;
			}
		} else {
			safetyTarget = null;
			atSafetyTarget = false;
		}

		updateScores(idleFlyScore, flyToDestScore, eatScore, sitScore, nestScore, hideScore);

		// Want to eat
		if (Helpers.IsMax(eatScore, stateScore)) {

			if (foodTarget == null) {
				atFoodTarget = false;
			}
			// look for food
			Transform food = Helpers.FindNearestFromArray(pos.x, pos.y, findFoodTargets());
			if (food) {
				if (atFoodTarget) {
					// already at the food
					flyToDestScore = 0.0;
					foodTarget = food.gameObject;
				} else {
					// fly to food
					foodTarget = food.gameObject;
					flyToDestScore = eatScore;
					nextState = STATE.EAT;
					eatScore = 0.0;
					nextDestination.x = food.position.x;
					nextDestination.y = food.position.y+0.15f;
				}
			} else {
				// no food for hungry birb
				eatScore = 0;
			}

		}

		updateScores(idleFlyScore, flyToDestScore, eatScore, sitScore, nestScore, hideScore);

		// If no score is greater than 0.4 by now then choose an idle state
		if (Helpers.IsMax(0.4, stateScore)) {
			// check if already flying to perform idle state
			if (state == STATE.FLYTODEST) {
				if (nextState == STATE.SIT) {
					sitScore = 0.5;
				} else if (nextState == STATE.NEST) {
					// already chose to nest
					nestScore = 0.5;
				}
			// fly more during the day
			} else if (chance(time.getDay() ? 70 : 30)) {
				idleFlyScore = 0.5;
			// can only nest if have birdbox
			} else if (chance(75) || birdbox.getLevel() < 1) {
				sitScore = 0.5;
			} else {
				nestScore = 0.5;
			}
		}

		updateScores(idleFlyScore, flyToDestScore, eatScore, sitScore, nestScore, hideScore);

		// Want to sit
		if (Helpers.IsMax(sitScore, stateScore)) {
			// look for something to sit on
			Transform fence;
			if (sitTarget != null) {
				fence = sitTarget.transform;
			} else {
				fence = Helpers.ChooseRandomGameObjectWithTags( new string[] {"Fence", "Hedge"}, rnd).transform;
				sitTarget = fence.gameObject;
			}

			if (atSitTarget) {
				// already there
				flyToDestScore = 0.0;
			} else {
				// fly to seat
				flyToDestScore = sitScore;
				nextState = STATE.SIT;
				sitScore = 0.0;
				nextDestination.x = fence.position.x;
				nextDestination.y = fence.position.y+0.47f;
			}
		}

		// Want to nest
		if (Helpers.IsMax(nestScore, stateScore)) {
			if (nestTarget == null) {
				nestTarget = birdbox.gameObject;
			}

			if (atNestTarget) {
				flyToDestScore = 0.0;
			} else {
				flyToDestScore = nestScore;
				nextState = STATE.NEST;
				nestScore = 0.0;
				nextDestination = nestTarget.transform.position;
			}

		}

		updateScores(idleFlyScore, flyToDestScore, eatScore, sitScore, nestScore, hideScore);

		// update destination
		destination.x = nextDestination.x;
		destination.y = nextDestination.y;

		// set state from scores
		state = (STATE) Helpers.IndexOfMax(stateScore);

		// if not flying somewhere then no destination
		if (state != STATE.FLYTODEST) {
			nextState = STATE.NULL;
		}

		// if fleeing from predator go faster based on hunger level
		if (nextState == STATE.HIDE) {
			maxSpeed = maxRunSpeed - maxIdleSpeed * (float)hunger;
		} else {
			maxSpeed = maxIdleSpeed;
		}

		// reset targets so a new one is chosen next time
		if (state != STATE.SIT && state != STATE.FLYTODEST) sitTarget = null;
		if (state != STATE.EAT && state != STATE.FLYTODEST) foodTarget = null;
		if (state != STATE.HIDE && state != STATE.FLYTODEST) safetyTarget = null;

		// lock the state if performing idle actions
		if (state == STATE.SIT || state == STATE.IDLEFLY) {
			lockFrames = 600;
			if(!time.getDay() && state==STATE.IDLEFLY) {
				// don't fly for too long at night
				lockFrames = 200;
			}
		}
		if (state == STATE.HIDE || state == STATE.NEST) {
			lockFrames = 400;
		}
	}

	/* Updates the stateScore array with given scores */
	void updateScores(double idleFlyScore, double flyToDestScore, double eatScore, double sitScore, double nestScore, double hideScore) {
		stateScore[(int) STATE.IDLEFLY] = idleFlyScore;
		stateScore[(int) STATE.FLYTODEST] = flyToDestScore;
		stateScore[(int) STATE.EAT] = eatScore;
		stateScore[(int) STATE.SIT] = sitScore;
		stateScore[(int) STATE.NEST] = nestScore;
		stateScore[(int) STATE.HIDE] = hideScore;
	}

	/* Select behaviour based on current state */
	void actOnState() {
		switch (state) {
			case STATE.IDLEFLY:
				idleFly();
				break;
			case STATE.FLYTODEST:
				flyToDest();
				break;
			case STATE.EAT:
				eat();
				break;
			case STATE.SIT:
				sit();
				break;
			case STATE.HIDE:
				hide();
				break;
			case STATE.NEST:
				nest();
				break;
			default:
				break;
		}
	}

	/* Idle flying behaviour of sparrow
	   Fly naturally constrained within a box */
	void idleFly() {
		flying = true;
		safe = false;

		// if outside area then turn around
		if (pos.x > maxPos.x || pos.x < minPos.x) {
			direction.x = pos.x > maxPos.x ? -1 : 1;
		}

		// changing direction already
		if (Mathf.Sign(speed.x) != direction.x) {
			speed.x += 0.001f * direction.x;
		} else {
			// if going less than half max speed then speed up
			// also chance to speed up after
			if (Math.Abs(speed.x) < maxSpeed/2 || chance(5)) {
				speed.x += 0.001f * Mathf.Sign(speed.x);
			}
			// % chance to change direction at any point
			if (chance(0.3)) {
				direction.x *= -1;
			}
		}

		// if outside area then turn around
		if (pos.y > maxPos.y || pos.y < minPos.y) {
			direction.y = (pos.y > maxPos.y ? -1 : 1);
		}

		// changing direction already
		if (Mathf.Sign(speed.y) != direction.y) {
			this.speed.y += 0.001f * direction.y;
		} else {
			// if going less than half max speed then speed up
			// also chance to speed up after
			if (Math.Abs(speed.y) < maxSpeed/2 || chance(5)) {
				this.speed.y += 0.001f * Mathf.Sign(speed.y);
			}
			// % chance to change direction at any point
			if (chance(0.3)) {
				direction.y *= -1;
			}
		}

		correctSparrowSpeed();
		transform.position += new Vector3(speed.x, speed.y, 0);
	}

	/* flying to destination behaviour of sparrow
	   changes direction and increases/decreases speed
	   naturally towards destination target */
	void flyToDest() {
		flying = true;
		safe = false;

		// if going in wrong x direction
		// accelerate in opposite direction
		if ((pos.x - destination.x) * speed.x >= 0) {
			if (pos.x - destination.x > 0.3f) {
				speed.x -= 0.001f;
			} else if (pos.x - destination.x < -0.3f) {
				speed.x += 0.001f;
			}
		} else {
			// increase up to max speed if not close by
			// if close then decrease the max speed (sorry)
			float dynamicSpeed = Math.Abs(pos.x - destination.x) < 1f ? (Math.Abs(pos.x - destination.x) < 0.1f ? maxSpeed * 0.1f : (maxSpeed/1.5f) * Math.Abs(pos.x - destination.x)) : maxSpeed/1.5f;

			if (dynamicSpeed < minSpeed) {
				dynamicSpeed = minSpeed;
			}

			if (Math.Abs(speed.x) < dynamicSpeed) {
				speed.x += 0.001f * Mathf.Sign(speed.x);
			} else if (Math.Abs(speed.x) > dynamicSpeed + 0.002f) {
				speed.x -= 0.001f * Mathf.Sign(speed.x);
			}

		}

		// if going in wrong pos.y direction
		if ((pos.y - destination.y) * speed.y >= 0) {
			// accelerate in opposite direction
			if (pos.y - destination.y > 0.3f) {
				speed.y -= 0.001f;
			} else if (pos.y - destination.y < -0.3f) {
				speed.y += 0.001f;
			}
		} else {
			// increase up to max speed if not close by
			// if close then decrease the max speed (sorry)
			float dynamicSpeed = Math.Abs(pos.y - destination.y) < 1f ? (Math.Abs(pos.y - destination.y) < 0.1f ? maxSpeed * 0.1f : (maxSpeed/1.5f) * Math.Abs(pos.y - destination.y)) : maxSpeed/1.5f;

			if (dynamicSpeed < minSpeed) {
				dynamicSpeed = minSpeed;
			}

			if (Math.Abs(speed.y) < dynamicSpeed) {
				speed.y += 0.001f * Mathf.Sign(speed.y);
			} else if (Math.Abs(speed.y) > dynamicSpeed + 0.002f) {
				speed.y -= 0.001f * Mathf.Sign(speed.y);
			}

		}

		correctSparrowSpeed();
		transform.position += new Vector3(speed.x, speed.y, 0);
	}

	/* eating behaviour of sparrow
	   every 10 frames then the foodtarget
	   is eaten */
	void eat() {
		flying = false;
		safe = false;

		speed.x = 0;
		speed.y = 0;
		if (atFrame(10)) {
			if (foodTarget != null) {
				// if food is left
				Food food = foodTarget.GetComponent<Food>();
				if (food.hasFood()) {
					hunger -= food.Eat();
				}
			}
		}

	}

	/* sitting behaviour of sparrow */
	void sit() {
		flying = false;
		safe = false;
		speed.x = 0;
		speed.y = 0;
	}

	/* hiding behaviour of sparrow */
	void hide() {
		flying = false;
		safe = true;
		speed.x = 0;
		speed.y = 0;
	}

	/* Nesting behaviour of sparrow */
	void nest() {
		flying = false;
		safe = true;
		speed.x = 0;
		speed.y = 0;
	}

	/* don't exceed the speed limit */
	void correctSparrowSpeed() {
		if (Math.Abs(speed.x) > maxSpeed) {
			speed.x = maxSpeed * Mathf.Sign(speed.x);
		}
		if (Math.Abs(speed.y) > maxSpeed) {
			speed.y = maxSpeed * Mathf.Sign(speed.y);
		}
	}

	/* Changes sparrow's animation the match current behaviour */
	void changeSpriteAnimation() {
		// hide the sparrow when safe
		if (state == STATE.NEST || (isSafe() && safetyTarget == birdbox.gameObject)) {
			GetComponent<SpriteRenderer>().enabled = false;
		} else {
			GetComponent<SpriteRenderer>().enabled = true;
		}

		// if hiding anywhere other than birdbox show head peeking out
		if (isSafe()) {
			animator.Play("maleSparrowHiding");
		}

		// choose animation
		else if (flying) {
			if (speed.x > 0 && speed.x > Math.Abs(speed.y)) {
				facing = Direction.EAST;
				animator.Play("maleSparrowFlyingRight");
			} else if (speed.x < 0 && -speed.x > Math.Abs(speed.y)) {
				facing = Direction.WEST;
				animator.Play("maleSparrowFlyingLeft");
			} else if (speed.y > 0 && speed.y >= Math.Abs(speed.x)) {
				facing = Direction.NORTH;
				animator.Play("maleSparrowFlyingUp");
			} else if (speed.y < 0 && -speed.y >= Math.Abs(speed.x)) {
				facing = Direction.SOUTH;
				animator.Play("maleSparrowFlyingDown");
			}
		} else {
			switch (facing) {
				case Direction.NORTH:
					animator.Play("maleSparrowSittingUp");
					break;
				case Direction.EAST:
					animator.Play("maleSparrowSittingRight");
					break;
				case Direction.SOUTH:
					animator.Play("maleSparrowSittingDown");
					break;
				case Direction.WEST:
					animator.Play("maleSparrowSittingLeft");
					break;
				default:
					break;
			}
		}
	}

	/* Adjusts sparrows stats over time based on their current state */
	void updateStats() {

		if (atFrame(100)) {
			// flying makes birds more hungry than sitting
			if (state == STATE.IDLEFLY || state == STATE.FLYTODEST) {
				hunger += 0.01;
			} else if (state != STATE.EAT) {
				hunger += 0.005;
			} else {
				// happy when eating
				happiness += 0.005;
			}
		}

		if (atFrame(150)) {
			// increase health if well fed
			if (hunger < 0.5) {
				health += 0.01;
				if (hunger < 0.2) {
					health += 0.01;
				}
			}
			// decrease health and happiness if starving
			else if (hunger >= 0.8) {
				health -= 0.01;
				happiness -= 0.01;
			}
			// still not being fed so very sad
			if (hunger >= 0.9) {
				happiness -= 0.02;
			}

		}

		// sparrows are happy to be alive for a long time
		if (atFrame(1000)) {
			happiness += 0.01;
		}

		// sad when fleeing from predators
		if (atFrame(20) && nextState == STATE.HIDE) {
			happiness -= 0.01;
		}

		// check the garden for nice things and reward player with coins
		if (atFrame(5000)) {
			calculateHappiness();
		}

		correctStats();
	}

	/* Kills the sparrow and notifies player with the given message */
	public void kill(string message) {
		notifs.addNotification(message, 0, 8);
		// spawn puff of smoke
		Instantiate(smokebomb, this.transform.position, transform.rotation = Quaternion.identity);
		Destroy(this.gameObject);
	}

	/* checks the garden for positive/negavtive items for sparrows
 	   and increases/decreases hapiness. If happiness above threshhold
	   then coins are earned */
	void calculateHappiness() {
		if (happiness >= 0.90) {
			Balance bal = GameObject.FindWithTag("Balance").GetComponent<Balance>();
			bal.increase(100);
			// if wasn't super dooper happy already tell the player how great they are
			if (!veryhappy) {
				notifs.addNotification(sparrowName +  " is extremely happy, keep it up!", 3, 6);
			}
			veryhappy = true;
		} else {
			veryhappy = false;
		}

		double delta = 0;

		int numFlowers = GameObject.FindGameObjectsWithTag("Flowers").Length;
		int numHedges = GameObject.FindGameObjectsWithTag("Hedge").Length;
		int numFences = GameObject.FindGameObjectsWithTag("Fence").Length;
		int numFood = GameObject.FindGameObjectsWithTag("Edible").Length;

		delta += 0.01 * numFlowers;
		delta += 0.01 * numHedges;
		delta -= 0.0025 * numFences;
		delta += 0.01 * numFood;

		happiness += delta;
	}

	/* keep stats in range [0,1] */
	void correctStats() {
		hunger = hunger < 0.0 ? 0.0 : hunger;
		hunger = hunger > 1.0 ? 1.0 : hunger;
		health = health < 0.0 ? 0.0 : health;
		health = health > 1.0 ? 1.0 : health;
		happiness = happiness < 0.0 ? 0.0 : happiness;
		happiness = happiness > 1.0 ? 1.0 : happiness;
	}

	/* has percent chance of returning true where
	   percent between 0 and 100 */
	private bool chance(double percent) {
		return Random.value * 100 < percent;
	}

	/* returns true if current frame is divisble by x
	   used for doing things every x frames */
	private bool atFrame(int x) {
		return frame % x == 0;
	}

	/* returns an array of all GameObjects tagged with 'Edible'
	   and have a Food component with current supply > 0 */
	GameObject[] findFoodTargets() {
		List<GameObject> available = new List<GameObject>();
		GameObject[] foods = GameObject.FindGameObjectsWithTag("Edible");
		foreach (GameObject food in foods) {
			if (food.GetComponent<Food>().hasFood()) {
				available.Add(food);
			}
		}
		return available.ToArray();
	}

	/* returns an array of GameObjects sparrow can HIDE in,
	   currently hedges and birdbox */
	GameObject[] findSafetyTargets() {
		List<GameObject> targets = new List<GameObject>();
		GameObject[] hedges = GameObject.FindGameObjectsWithTag("Hedge");
		foreach (GameObject hedge in hedges) {
			targets.Add(hedge);
		}
		if (birdbox.getLevel() > 0) {
			targets.Add(birdbox.gameObject);
		}
		return targets.ToArray();
	}

}
