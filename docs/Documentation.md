# Development Guide

This is a reference guide for the development of Sparrow Quest written by the CSL development team.
During the development of Sparrow Quest we are constantly learning new and more complex development techniques like making sprites, animations, menus, etc.
and here is where you will find guides, examples, case-histories, and other useful files made by us and also links to other great resources by Unity!

Please clone or download the repository to make contributions.

## Setup

Unity3d is required to make any contributions to the game, the link to download can be found [here](https://unity3d.com/).

After the installation, run unity and click on Open a Project.
The Sparrow Game directory can be found at /client/

![alt text][game]

[game]:https://puu.sh/zKoec/160ace6b16.png

Please make sure you are able to build and run the game before proceeding further by clicking "Play" located at top center of the screen.

### 1. Scripts

The game uses and focuses heavily on scripts written in [C#](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/) located in ../SparrowGame/Assets/Scripts.

Tutorials on basic Unity scripting, syntaxes and some useful functions can be found [here](https://unity3d.com/learn/tutorials/s/scripting).

### 2. Sprites

This folder contains all of the sprites which are used in the game.

The sprites were created using [Piskel](https://www.piskelapp.com/) and Adobe Photoshop.

### 3. Resources

This folder contains all of our prefabs needed for the game. A [prefab](https://docs.unity3d.com/Manual/Prefabs.html) stores a GameObject complete with components and properties.
Generally it is a good idea to make a prefab of Gameobjects so they are saved separately from the Scene and cannot be individually editable.

### 4. Scenes

Currently we have two scenes which run the game. The first is a login scene which always runs first followed by the main scene.

The login scene runs DataController which when you enter your credentials, will communicate with the server and checks if they are valid and will then load your user data (if any).
After the server has responded and data has been loaded, the "main" scene would be called.

The main scene has all the gameobjects and behaviours which help our game run and create gameplay.

### 5. Animations

Animations are created with unity using the animation tab and sprites.

![alt text][anim]

[anim]:https://puu.sh/zKok9/7b6e82b386.png

The animation [guide](https://unity3d.com/learn/tutorials/s/animation?_ga=2.129338180.992749909.1521392037-237382043.1520432195)
from unity helped us create basic animations which blends in our pixel art theme.

### 6. Pallets and Tiles

The pallet folder contains our automated map generation sprites and prefabs.

The tile folder contains tiling objects like fences, roads, etc.

### Testing

As we are using Unity it is difficult to unit test components in isolation, as they are often dependent on other parts of the game. Instead we opted to use User Acceptance Tests to identify bugs and user experience smells.

The full documentation on it can be found at docs/UAT.md
