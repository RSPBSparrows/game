# Release notes

## Iteration 1

* Sparrows can fly around the garden
* Static background

![Iteration 1](../screenshots/iteration1.png)

## Iteration 2

* Sparrows are more lifelike- can eat seeds from the ground
* Changes to the background

## Iteration 3

* Shop and Inventory UI added, without functionality currently
* Sparrows now sit on fences

![Iteration 3](../screenshots/OriginalUI.png)

## Iteration 4

* Birdbox can be bought and upgraded to house more sparrows
* Predators added to the garden
* Grass will grow over time

## Iteration 5

* Night and day added
* UI looks updated
* Login via a housesparrowcience.com account to save data locally and on the server
