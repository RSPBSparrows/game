# User Acceptance Testing
### Template
1.  Plan test
    *  choose functionality
    *  define task(s)
2.  Do test with fresh user and take results
    *  note unexpected software behavior (steps to produce bugs)
    *  note user navigation/understanding issues
3.  Analyse results, what worked/didn't work, why?

4.  Make Issues for problem and fix them



### 1. #39 [UAT] Zoom
1. Zooming
    - Pinch zoom in
    - Pinch zoom out
2. Test:
    - Expected:
        - Game zooms in
    - Actual
        - Game doesn't zoom
3. Outcome:
    - Make issue #39

### 2. [UAT] Purchase something from store
1. Buy something from the store
    - Buy seeds from the store
    - Check your inventory
2. Test:
    - Expected:
        - When seeds are selected, coins should decrease by 10
        - Seeds should now appear in the inventory
    - Actual:
        - Coins does decrease by 10
        - When inventory is selected, seeds are appear
        - But inventory panel appears on top of shop instead of closing shop and opening inventory
        - User can move the map by dragging in the UI
        - User accidentally clicked on birdbox through the inventory panel
3. Outcome:
    - Issues #40 #37 #38


### 3. [UAT] Close App
1. Close the app
    - Press back to close
2. Test:
    - Expected:
        - When pressed back, the game should close
    - Actual:
        - The app didn't close
        - When locked and then unlocked the game is still on top
3. Outcome:
    - Issues #36 #45

### 4. [UAT] Add items to the map
1. From the inventory, given that "seeds" is already in the inventory
    - Click and drag the item from the inventory to the map
2. Test:
    - Expected:
        - Selected item should appear on top of the user's cursor/finger
        - The item is placed where ever user releases the cursor/finger
    - Actual:
        - When dragging the map moves
        - Aren't able to drag item onto map
3. Outcome:
    - Issue #30

### 5. [UAT] Scare away predators to save your sparrows
1. Shoo away predators
    - On the main game screen wait for a cat to appear
    - When the cat appears tap on it to shoo it away saving your sparrows
2. Test:
    - Expected:
        - When the cat is clicked on it should leave the garden and disappear
    - Actual:
        - Cat keeps chasing sparrows until they all are caught
        - Sometimes the sparrows escape but are then trapped in their box as the cat doesn't leave
3. Outcome:
    - Updated issue #27

### 6. [UAT] Check the status of your sparrows
1. Sparrow status
    - On the main game screen select a birdbox to open the birdbox panel
    - View the bars on the right side of the panel.
2. Test:
    - Expected:
        - As the sparrows fly around their food bar slowly declines
        - If they are eating then it should increase quicker
        - If their hunger gets very low their health should start declining
    - Actual:
        - The bars display the sparrow stats changing correctly
3. Outcome:
   - Test passed

### 7. [UAT] Name your sparrows
1. Naming sparrows
    - On the main game screen select a birdbox to open the birdbox panel
    - Select the name of a sparrow and change it
    - Close the birdbox panel
    - Reopen birdbox panel
2. Test:
    - Expected:
        - When the name of a sparrow is selected then you are able to type a new name
        - After the panel is closed and reopened the new name should persist
    - Actual
        - Sparrows could be named and would persist through the box closing
        - User had problems closing the sparrow box as it can only be closed by clicking through the panel onto the box again
3. Outcome:
    - Updated issue #40

### 8. [UAT] Scare away the predator to protect sparrows

1. Shoo away predators
    - On the main game screen wait for a cat to appear
    - When the cat appears tap on it to shoo it away saving your sparrows
2. Test:
    - Expected:
        - When the cat is clicked on it should leave the garden and disappear
    - Actual:
        - Flying cat - Users did not expect the cat to climb buildings so fast
        - Cat is "cuter" than sparrow - User decided to let the sparrows die and keep the cat instead.
        - Cat disappeared when clicked on, sparrows were saved.
3. Outcome:
     - Updated issue #27

### 9. [UAT] Take the sparrow quiz

1. Take the quiz and see if you can pass
   - Answer correctly which 1 of 4 is the house sparrow from 6 questions
2. Test:
   - Expected:
        - Users will pass if score is >70%
        - Random images and progressively harder questions.
   - Actual:
        - Users passed if score was above 70% but score showed a large floating value
        - Images were random but sometimes had same images showed up.
3. Outcome:
   - Fixed scoring system to round of
   - Made images have much better random chance and got an array for unused images.

### 10. [UAT] Grass cutting simulation

1. Cut the long grass
   - When the grass grows, click on it to cut it.
2. Test:
   - Expected:
        - The grass should become small again when clicked.
   - Actual:
        - Success, but too rapid.
3. Outcome:
    - Test passed.

### 11. [UAT] Close the game

1. Try to close the game
   - Press back button to close
   - Lock the screen and reopen

2. Test:
   - Expected:
        - The application will close when press back
        - The application will be under lock screen and will reopen when unlocked
        - An earlier issue was the game was on top of the lock screen
   - Actual:
        - The application does close: Success.
        - The application is under lock screen but turns black when screen is unlocked.

3. Outcome:
    - Updated issue #39 and issue #47

### 12. [UAT] Gameplay

1. Play the game
   - Play the game for 5 mins to find more bugs and give feedback
2. Test Feedback:
   - Lacks gameplay, the only interaction with the bird is with a box and that too only looking at stats and naming them
   - If the user can name the sparrow then have the sparrow display name above them.
   - Day/ Night cycle doesn't change anything expect making it dark
   - Initial sounds were good actually, the game should constantly have background music or at random intervals, is soothing.
   - If the game is about helping to prevent sparrows, give information about how things the user do can protect sparrow in real world. For eg: If buying hedges: how are they better in protecting sparrows and how cutting way short or letting them grow affects the sparrow.

3. Outcome:
    - Issue #49 sparrows have name bar
    - Updated issue #44 sparrows do less things at night and prefer to rest.
    - Issue #50 for sounds
    - Issue #51 Be more informative


### 13. #39 [UAT] Zoom
1. Zoom in smoothly
    - Pinch zoom in
    - Pinch zoom out
2. Test:
    - Expected:
        - Game zooms in smoothly
    - Actual
        - Game doesn't zoom smoothly
        - When leaving finger off screen, the camera jumps
        - Very sensitive when zooming
3. Outcome:
    - Issue #71


## New bugs

The following bugs are new and no issues have been created for them

### 14. [UAT] Item States

1. Replace items from inventory
      - Put already placed item in inventory
      - Place them from inventory

2. Test:
    - Expected:
        - Items are placeable with their previous states
     - Actual:
        - Items are placeable but with the inital states

### 15. [UAT] Duplicated hedges

1. Place hedges over fences

2. Test:
    - Expected:
        - Hedges are placeable
    - Actual:
        - Hedges are placeable but sometimes you get more hedges than you have

### 16. Fence behavior

Fences sometimes behave differently and breaks

### 17. Pinch zoom replaces Items

1. Pinch zooms

2. Test:
    - Expected:
        - Zooming works as Expected
    - Actual:
        - Pinch zooming works but if any finger is on a draggable item, the item gets in a state of being placed.
