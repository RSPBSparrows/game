/*
 * Server for the sparrowquest game
 * May need to add some form of anti-cheat to the 'balance' section as this may be used as a score system of sorts in future.
 */

// get environment variable for setup ('production', 'development' or 'test')
var environment = process.env.CODE_ENVIRONMENT;

// check environment variable is set
if (environment === undefined || !['test', 'development', 'production'].includes(environment)) {
  console.log("$CODE_ENVIRONMENT not set!\nConfigure web server with env variables\nExiting...");
  process.exit();
}

var mysql = require('mysql');
var bodyParser = require('body-parser');
var express = require("express");
var app = express();
var passport = require('../../authentication/auth/validate.js')();
var config = require('../../config.js')[environment]
var path = require("path");

// fetch the sequelize connection for this environment
var sequelize = require("../../authentication/db_setup/connection.js");
// the models module exports the DB models instance so we have
// to pass it our sequelize connection
var models = require("../../authentication/db_setup/models.js")(sequelize);

// secure non-string operators for sequelize
const Op = models.Op;

// multiplier for data-app score to game currency/balance
const profileScoreMultiplier = 3;

var GameBalance = models.GameBalance;
var GameGarden = models.GameGarden;
var GameInventory = models.GameInventory;
var GameSparrows = models.GameSparrows;
var UserProfile = models.UserProfile;

app.use(express.static(__dirname + '/build'));

app.use(bodyParser.urlencoded({
  extended: true
}));

app.use(passport.initialize());

// define the node app's id as 'game-server'
module.id = "game-server";

app.get("/", function(req, res) {
  res.sendFile(path.join(__dirname + '/build/index.html'));
});

//Load a garden from the server
app.get("/garden/", passport.authenticate('jwt', {
  session: false
}), function(req, res) {
  user_id = req.user.id

  if (!user_id) {
    res.status(401).send("Unauthorised");
    return;
  }

  GameGarden.findAll({
    where: {
      user_id: user_id
    }
  }).then((gardens) => {
    if (gardens.length == 1) {
      let response = { data: gardens[0].data };
      res.status(200).json(response);
      return;
    } else {
      res.status(404).send("Not found.");
    }
  }).catch(err => {
    console.log(err);
    res.status(500).json({
      message: "Server error."
    });
  });
});

//Save a garden to the server
app.post("/garden/", passport.authenticate('jwt', {
  session: false
}), function(req, res) {
  data = req.body.data
  user_id = req.user.id

  if (!user_id) {
    res.status(401).send("Unauthorised");
    return;
  }

  GameGarden.upsert({
    user_id: user_id,
    data: data
  }).then((created) => {
    let message = (created) ? "created" : "updated";
    res.status(200).send("Success, record " + message);
    return;
  }).catch(err => {
    console.log(err);
    res.status(500).json({
      message: "Server error."
    });
  });
});

//Load an inventory from the server
app.get("/inventory/", passport.authenticate('jwt', {
  session: false
}), function(req, res) {
  user_id = req.user.id

  if (!user_id) {
    res.status(401).send("Unauthorised");
    return;
  }

  GameInventory.findAll({
    where: {
      user_id: user_id
    }
  }).then((inventories) => {
    if (inventories.length == 1) {
      let response = { data: inventories[0].data };
      res.status(200).json(response);
      return;
    } else {
      res.status(404).send("Not found.");
    }
  }).catch(err => {
    console.log(err);
    res.status(500).json({
      message: "Server error."
    });
  });
});

//Save an inventory to the server
app.post("/inventory/", passport.authenticate('jwt', {
  session: false
}), function(req, res) {
  data = req.body.data
  user_id = req.user.id

  if (!user_id) {
    res.status(401).send("Unauthorised");
    return;
  }

  GameInventory.upsert({
    user_id: user_id,
    data: data
  }).then((created) => {
    let message = (created) ? "created" : "updated";
    res.status(200).send("Success, record " + message);
    return;
  }).catch(err => {
    console.log(err);
    res.status(500).json({
      message: "Server error."
    });
  });
});

//Load sparrow data from the server
app.get("/sparrows/", passport.authenticate('jwt', {
  session: false
}), function(req, res) {
  user_id = req.user.id

  if (!user_id) {
    res.status(401).send("Unauthorised");
    return;
  }

  GameSparrows.findAll({
    where: {
      user_id: user_id
    }
  }).then((sparrows) => {
    if (sparrows.length == 1) {
      let response = { data: sparrows[0].data };
      res.status(200).json(response);
      return;
    } else {
      res.status(404).send("Not found.");
    }
  }).catch(err => {
    console.log(err);
    res.status(500).json({
      message: "Server error."
    });
  });
});

//save sparrow data to the server
app.post("/sparrows/", passport.authenticate('jwt', {
  session: false
}), function(req, res) {
  data = req.body.data
  user_id = req.user.id

  if (!user_id) {
    res.status(401).send("Unauthorised");
    return;
  }

  GameSparrows.upsert({
    user_id: user_id,
    data: data
  }).then((created) => {
    let message = (created) ? "created" : "updated";
    res.status(200).send("Success, record " + message);
    return;
  }).catch(err => {
    console.log(err);
    res.status(500).json({
      message: "Server error."
    });
  });
});

//Get balance delta
app.get("/balance/", passport.authenticate('jwt', {
  session: false
}), function(req, res) {
  user_id = req.user.id

  if (!user_id) {
    res.status(401).send("Unauthorised");
    return;
  }

  GameBalance.findAll({
    where: {
      user_id: user_id
    }
  }).then((balances) => {
    if (balances.length == 1) {
      let response = { balance: balances[0].balance };
      res.status(200).json(response);
      return;
    } else {
      res.status(404).send("Not found.");
    }
  }).catch(err => {
    console.log(err);
    res.status(500).json({
      message: "Server error."
    });
  });
});

//Update balance (and score) with a delta
app.post("/balance/", passport.authenticate('jwt', {
  session: false
}), function(req, res) {
  var data = parseInt(req.body.data)
  var request_user_id = req.user.id

  if (!request_user_id) {
    res.status(401).send("Unauthorised");
    return;
  }

  if (data == null || data == NaN) {
    console.log("Invalid Data " + data)
    res.status(401).send("Invalid data");
    return;
  }

  GameBalance.findAll({
    where: {
      user_id: request_user_id
    }
  }).then((balances) => {
    UserProfile.findAll({
      where: {
        userProfileId: request_user_id
      }
    }).then((profiles) => {
      if (profiles.length == 1) {
        var profile_score = [profiles[0].profileScore, true];
      } else {
        var profile_score = [0, true];
      }
      if (balances.length == 1) {
        let old_balance = balances[0].balance;
        let old_cum_balance = balances[0].cum_balance;
        let last_score = balances[0].last_score;
        let score_increase = ((profile_score[0] > last_score) ? profile_score[0] - last_score : 0) * profileScoreMultiplier;
        let new_balance = old_balance + score_increase + data;
        let new_cum_balance = old_cum_balance + score_increase + ((data > 0) ? data : 0);
        GameBalance.update({
          balance: new_balance,
          cum_balance: new_cum_balance,
          last_score: profile_score[0]
        }, {
          where: {
            user_id: request_user_id
          }
        }).then(() => {
          GameBalance.findAll({
            where: {
              user_id: request_user_id
            }
          }).then((balances) => {
            if (balances.length == 1) {
              let response = { balance: balances[0].balance };
              res.status(200).json(response);
              return;
            } else {
              res.status(404).send("Not found.");
            }
          }).catch(err => {
            console.log(err);
            res.status(500).json({
              message: "Server error."
            });
          });
        }).catch(err => {
          console.log(err);
          res.status(500).json({
            message: "Server error."
          });
        });
      } else if (balances.length == 0) {
        let cum_balance = (data > 0) ? data : 0;
        if (profile_score[1]) {
          data += (profile_score[0] * profileScoreMultiplier);
          cum_balance += (profile_score[0] * profileScoreMultiplier);
        }
        GameBalance.create({
          balance: data,
          cum_balance: cum_balance,
          last_score: profile_score[0],
          user_id: request_user_id
        }).then(() => {
          GameBalance.findAll({
            where: {
              user_id: request_user_id
            }
          }).then((balances) => {
            if (balances.length == 1) {
              let response = { balance: balances[0].balance };
              res.status(200).json(response);
              return;
            } else {
              res.status(404).send("Not found.");
            }
          }).catch(err => {
            console.log(err);
            res.status(500).json({
              message: "Server error."
            });
          });
        }).catch(err => {
          console.log(err);
          res.status(500).json({
            message: "Server error."
          });
        });
      } else {
        res.status(404).send("Not found.");
      }
    }).catch(err => {
      console.log(err);
      res.status(500).json({
        message: "Server error."
      });
    });
  }).catch(err => {
    console.log(err);
    res.status(500).json({
      message: "Server error."
    });
  });
});

// hide x-powered-by header from responses
app.disable('x-powered-by');
app.listen(config.gameServer.port);
console.log("Game server listening on port", config.gameServer.port);
